package main

import (
	"fmt"
)

// 字符串的使用
func main() {
	// 定义一个字符串
	var s1 string = "你好全面拥抱Golang!"
	fmt.Println(s1)

	// 字符串是不可变的，指的是字符串一旦定义好，其中的字符的值不能改变
	var s2 string = "abc"
	s2 = "def"
	// s2[0] = 't' 
	fmt.Println(s2)

	// 字符串的表现形式
	// 如果字符串没有特殊字符，字符串的表示是用双引号
	var s3 string = "huanhuandenghong"
	fmt.Println(s3)
	// 有特殊字符，字符串的表示形式用反引号``
	var s4_ts string = `
	@fdjaslkj...\,\n\t
	\\\
	\n
	`
	fmt.Println(s4_ts)
	fmt.Println("------------------------------------------------------------")
	// 拼接
	var s5_p string = "lihuan" + "denghong"
	fmt.Println("s5_p=",s5_p)
	var s5_p01 string = "Java" + "Script"
	fmt.Println("s5_p01=",s5_p01)
	var s5_p02 string = "Linux" + " Nginx " + " MySQL " + "Web"
	fmt.Println("s5_p02=",s5_p02)

	fmt.Println("------------------------------------------------------------")
	// 默认值
	var a_int int
	var b_float32 float32
	var c_float64 float64
	var d_bool bool
	var e_string string
	fmt.Println("a_int的默认值是： ",a_int)
	fmt.Println("b_float32的默认值是： ",b_float32)
	fmt.Println("c_float64的默认值是： ",c_float64)
	fmt.Println("d_bool的默认值是： ",d_bool)
	fmt.Println("e_string的默认值是： ",e_string)
}