package main

import (
	"fmt"
	// "strconv"
	"time"
)

// 系统函数、内置函数,内置函数要仔细重新看
// 日期函数
func main() {
	// 返回子串在字符串第一次出现的索引值；
	// fmt.Println(string.Index("golangHuanRedang", "ang"))
	
	// 获取当前的日期
	now := time.Now()
	// fmt.Println()
	fmt.Printf("%v --- 对应的类型为： %T \n", now, now)
	fmt.Println()

	// 调用结构体中的方法
	fmt.Printf("年 = %v \n", now.Year())
	fmt.Printf("月 = %v \n", now.Month())
	fmt.Printf("月 = %v \n", int(now.Month()))
	fmt.Printf("日 = %v \n", now.Day())
	fmt.Printf("时 = %v \n", now.Hour())
	fmt.Printf("分 = %v \n", now.Minute())
	fmt.Printf("秒 = %v \n", now.Second())
	fmt.Printf("年-月-日 = %v ,%v,%v\n", now.Year(), int(now.Month()), now.Day())
	fmt.Printf("%v年 %v月 %v日 \n", now.Year(), int(now.Month()), now.Day())
	fmt.Printf("%v:%v:%v \n", now.Hour(), now.Minute(), now.Second())
	fmt.Println("------------------------------------------------------------")

	// 日期格式化
	// Printf将字符串直接输出
	fmt.Printf("当前年月日：%d-%d-%d 时分秒：%d:%d:%d \n", now.Year(), int(now.Month()),
	now.Day(),  now.Hour(), now.Minute(), now.Second())

	// Sprintf可以得到这个字符串，以便后续使用
	datestr := fmt.Sprintf("当前年月日：%d-%d-%d 时分秒：%d:%d:%d \n", now.Year(), int(now.Month()),
	now.Day(),  now.Hour(), now.Minute(), now.Second())
	fmt.Println(datestr)
	fmt.Println()

	// 按照指定格式，参数字符串的各个数字必须是固定的
	datestr2 := now.Format("2006/01/02 15/04/05")
	fmt.Println(datestr2)
	fmt.Println()

	// 选择任意的组合都是可以的，根据需求自己选择就可以（任意组合）
	datestr3 := now.Format("2006 15:04")
	fmt.Println(datestr3)
	fmt.Println()

	// 月 日 时间
	date_01 := now.Format("01/02 15:04")
	fmt.Println(date_01)
	fmt.Println()

	// 时间
	date_02 := now.Format("15:04")
	fmt.Println("现在的时间是：", date_02)
	fmt.Println()
}