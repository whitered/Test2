package main

import "fmt"

var (
        n9 = 500
        n10 = "netty"
)

func main() {
	fmt.Println("hello Golang!")
	fmt.Println("hello Golang!")
	// 变量的声明
	var age int
	// 变量的赋值
	age = 18
	// 变量的使用
	fmt.Println("age = ", age)

	// 声明和赋值可以合成一句
	var age2 int = 19
	fmt.Println("age2 = ", age2)

	// 第一种方式
	var num int = 18
	fmt.Println("num = ", num)

	// 第二种，指定变量的类型，但是不赋值，使用默认值
	var num2 int
	fmt.Println("num2 = ", num2)

	// 第三种，如果没有写变量的类型，那么根据=后面的值进行判定变量的类型（自动类型推断）
	var num3 = "tom"
	fmt.Println("num3 = ", num3)
	
	// 第四种：省略var,注意 := 不能写为 = 
	sex := "男"
	fmt.Println("sex = ", sex)

	fmt.Println("------------------------------------------------------------")
	var n1, n2, n3 int
	fmt.Println("n1 = ", n1)
	fmt.Println("n2 = ", n2)
	fmt.Println("n3 = ", n3)

	var n4, name, n5 = 10, "jack", 7.8
	fmt.Println("n4 = ", n4)
	fmt.Println("name = ", name)
	fmt.Println("n5 = ", n5)

	n6, height := 6.9, 100
	fmt.Println("n6 = ", n6)
	fmt.Println("height = ", height)

	// 输出全剧变量
	fmt.Println("n9 = ", n9)
	fmt.Println("n10 = ", n10)
}