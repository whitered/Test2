package main

import "fmt"

func main() {
	//匿名函数
	a := func() {
		fmt.Println("Hello World first class function")
	}
	a()
	fmt.Printf("%T\n", a)

	//不调用，不传参数
	func() {
		fmt.Println("Hello World second class function")
	}()

	//传递参数
	func(n string) {
		fmt.Println("Hello World third class function", n)
	}("Gophers")

	//定义匿名函数，定义的同时调用
	fmt.Println("---------------------------------------------")
	result := func(num1 int, num2 int) int {
		return num1 + num2
	}(10, 20)
	fmt.Println(result)

	result01 := func(num1 int, num2 int) int {
		return num1 - num2
	}(100, 20)
	fmt.Println(result01)

	resultAdd := func(num1 int, num2 int) int {
		return num1 + num2
	}
	add01 := resultAdd(10, 20)
	fmt.Println(add01)

	resultSub := func(num1 int, num2 int) int {
		return num1 - num2
	}
	sub01 := resultSub(100, 20)
	fmt.Println(sub01)

	//max(10, 20)
	fmt.Println(max(10, 20))
	fmt.Println(max(110, 20))

}

func max(a, b int) int {
	var result int
	if a > b {
		result = a
	} else {
		result = b
	}
	return result
}
