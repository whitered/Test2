package main

import "fmt"

func main()  {
	fmt.Println("Hello Golang!")
	fmt.Println("Hello Golang!!!")
	fmt.Println("Hello Golang!")
	var age = 18 + 10
	fmt.Println("age = ",age)

	fmt.Println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				"bbbbbbbbbbbbbbbbbbbbbbbbbbbb",
				"cccccccccccccccccccccccccccc",
			    "dddddddddddddddddddddddddddd",
)
	var age01 int = 36
	fmt.Println("age01 = ",age01)

	// 新的开始
	// 变量的声明
	var age02 int
	// 变量的使用
	age02 = 18
	// 变量的使用
	fmt.Println("age02 = ", age02)

	// 声明和赋值可以合成一句：
	var age03 int = 37
	fmt.Println("age03 = ", age03)

	// 指定变量的类型，并且赋值
	var num int = 38
	fmt.Println("num = ", num)

	// 指定变量的类型，但是不赋值，使用默认值
	var num02 int
	fmt.Println("num02=",num02)

	// 如果没有写变量的类型，那么根据=后面的值进行判定变量的类型（自动类型推断）
	var num03 = "tom"
	fmt.Println("num03 = ",num03)

}