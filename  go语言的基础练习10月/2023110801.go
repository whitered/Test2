package main

import(
	"fmt"
	"unsafe"
)

func main()  {

	// go的整数类型，默认声明为int类型
	var num = 28
	// Printf函数的作用就是：格式化的，把num的类型填充到%T 的位置上
	fmt.Printf("num的类型是： %T", num)		// num的类型是：int
	fmt.Println()

	// 定义一个整数类型
	var num1 int8 = 120
	fmt.Println("num1 = ", num1)

	var num2 uint8 = 200
	fmt.Println("num2 = ", num2)

	var num3 = 28
	fmt.Printf("num3的类型是： %T ",num3)
	fmt.Println()

	fmt.Println(unsafe.Sizeof(num3))

}