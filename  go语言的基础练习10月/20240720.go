package main

import (
	"fmt"
	"unsafe"
)

func main() {
	//1、声明变量
	var age int
	var name string
	var class string
	var coding string

	//2、变量赋值
	age = 36
	name = "shuangWhite"
	class = "计算机"
	coding = "golang"
	//3、变量的使用
	fmt.Println(age, name, class, coding)
	fmt.Println()

	// 类型和占用字节数
	fmt.Printf("age的类型是： %d \t ，", age)
	fmt.Println("占字节数：", unsafe.Sizeof(age), "\n")
	fmt.Printf("name的类型是： %s \t ，", name)
	fmt.Println("占字节数：", unsafe.Sizeof(name), "\n")
	fmt.Printf("class的类型是 %s \t ，", class)
	fmt.Println("占字节数：", unsafe.Sizeof(class), "\n")
	fmt.Printf("coding类型是 %s \t ，", coding)
	fmt.Println("占字节数：", unsafe.Sizeof(coding), "\n")
	fmt.Println("------------------------------------------------------------")

	//浮点数
	//定义浮点数类型
	var num1 float32 = 3.14
	fmt.Println(num1)
	//var num2 float64 = 3.14
	//浮点数可以表示正浮点数，也可以表示负的浮点数
	var num2 float32 = -3.14
	fmt.Println("负浮点数：", num2)
	//浮点数可以用十进制表示形式，也可也用科学计数法表示形式 E大写小写都可以
	var num3 float32 = 314e-2
	fmt.Println("科学计数法：", num3)
	var num4 float32 = 314e+2
	fmt.Println("科学计数法：", num4)
	var num5 float32 = 314e-2
	fmt.Println("科学计数法：", num5)
	var num6 float32 = 314e+2
	fmt.Println("科学计数法：", num6)
	fmt.Println()

	//浮点数可能会有精度的损失，所以通常情况下，建议使用：float64()
	var num7 float32 = 256.000000916
	fmt.Println(num7)
	var num8 float64 = 256.000000916
	fmt.Println(num8)

	fmt.Println()
	var num9 = 3.154
	fmt.Println(num9)
	fmt.Printf("num9类型是: %T\n", num9)
	fmt.Println("占字节数：", unsafe.Sizeof(num9))

}
