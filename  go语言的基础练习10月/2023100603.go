package main

import (
	"fmt"
)

func main() {
	// 测试布尔类型的数值：
	var flag01 bool = true
	fmt.Println("flag01 = ", flag01)

	var flag02 bool = false
	fmt.Println("flag02 = ", flag02)

	var flag03 bool = 5 < 9
	fmt.Println(flag03)

	// 1、定义一个字符串：
	var s1 string = "你好全面拥抱Golang"
	fmt.Println("s1 = ", s1)
	// 2、字符串是不可变的，指的是字符串一旦定义好，其中的字符串的值不能改变
	var s2 string = "abc"
	fmt.Println("s2 = ", s2)

	// 3、字符串的表示形式：
	// （1）如果字符串中没有特殊字符，字符串的表示形式用双引号
	//  (2)如果字符串中有特殊字符，字符串的表示形式用反引号''
	var s3 string = "jdfajlfalkfdjlffjlkj"
	fmt.Println(s3)
	var s4 string = `
	// 1、定义一个字符串：
	var s1 string = "你好全面拥抱Golang"
	fmt.Println("s1 = ", s1)
	// 2、字符串是不可变的，指的是字符串一旦定义好，其中的字符串的值不能改变
	var s2 string = "abc"
	fmt.Println("s2 = ", s2)
	`
	fmt.Println(s4)

	// 4、字符串的拼接效果
	var s5 string = "abc" + "def"
	s5 += "hijk"
	fmt.Println(s5)

	fmt.Println("--------------------------------------------------")
	// 当一个字符串过长的时候；注意： +保留在上一行的最后
	var s6 string = "abc" + "def" + "abc" + "def" + "abc" + "def" + 
	"abc" + "def" + "abc" + "def" + "abc" + "def" + "abc" + "def" + 
	"abc" + "def" + "abc" + "def"
	fmt.Println(s6)

	var a int
	var b float32
	var c float64
	var d bool
	var e string
	fmt.Println("a = ", a)
	fmt.Println("b = ", b)
	fmt.Println("c = ", c)
	fmt.Println("d = ", d)
	fmt.Println("e = ", e)

	fmt.Println("进行类型转换")
	// 进行类型转换
	var n1 int = 100
	fmt.Println("n1 = ", n1)
	var n2 float32 = float32(n1)
	fmt.Println(n2)
	// 注意：n1的类型其实还有int类型，只是将n1的值100转为了float32而已，n1还是int的类型
	fmt.Printf("%T", n1)	// int
	fmt.Println()

	// 将int64转为int8的时候，编译不会出错的，但是会数据的溢出
	var n3 int64 = 888888
	var n4 int8 = int8(n3)
	fmt.Println("n4 = ", n4)			// 56
	fmt.Println("n3 = ", n3)			// 888888

	var n5 int32 = 12
	var n6 int64 = int64(n5) + 30		// 一定要匹配 = 左右的数据类型
	fmt.Println("n5 = ", n5)
	fmt.Println("n6 = ", n6)

	var n7 int64 = 12
	var n8 int8 = int8(n7) + 127		// 编译通过，但是结果可能会溢出
	// var n9 int8 = int8(n7) + 128		// 编译不会通过
	fmt.Println("n8 = ", n8)
	// fmt.Println("n9 = ", n9)

	fmt.Println("----------------------------------------------------")
	var num01 int = 19
	var num02 float32 = 4.78
	var num03 bool = false
	var num04 byte = 'a'

	var str1 string = fmt.Sprintf("%d", num01)
	fmt.Printf("str1对应的类型是: %T , str1 = %q \n", str1, str1)

	var str2 string = fmt.Sprintf("%f", num02)
	fmt.Printf("str2对应的类型是: %T , str2 = %q \n", str2, str2)

	var str3 string = fmt.Sprintf("%t", num03)
	fmt.Printf("str3对应的类型是: %T , str3 = %q \n", str3, str3)

	var str4 string = fmt.Sprintf("%c", num04)
	fmt.Printf("str4对应的类型是: %T , str4 = %q \n", str4, str4)

}