package main

import (
	"fmt"
)

	// 全局变量：定义在函数外的变量
	var n7 = 100
	var n8 = 9.7

	// 一次性声明的方式
	var (
		n9 = 500
		n10 = "netty"
	)

func main() {
	// 1、变量的声明
	var age int
	// 2、变量的赋值
	age = 18
	// 3、变量的使用
	fmt.Println("age = ", age)

	// 声明和赋值可以合成一句：
	var age2 int = 19
	fmt.Println("age2 = ", age2)

	// 不可以在赋值的时候给与不匹配的类型
	var num float32 = 12.56
	fmt.Println("num = ", num)

	// 第一种：变量的使用方式：指定变量的类型，并且赋值，
	var num01 int = 18
	fmt.Println("num01 = ", num01)

	// 第二种:指定变量的类型，但是不赋值，使用默认值
	var num02 int
	fmt.Println("num02 = ", num02)

	// 第三种：如果没有写变量的类型，那么根据=后面的值进行判定变量的类型（自动类型推断）
	var num03 = "tom"
	fmt.Println("num03 = ", num03)

	// 第四种：省略var, 注意  ：= 不能写为 = 
	sex := "男"
	fmt.Println("sex = ", sex)

	fmt.Println("---------------------------------------------------------------")
	// 声明多个变量：
	var n1, n2, n3 int
	fmt.Println("n1 = ", n1)
	fmt.Println("n2 = ", n2)
	fmt.Println("n3 = ", n3)

	var n4, name, n5 = 10, "jack", 7.8
	fmt.Println("n4 = ", n4)
	fmt.Println("name = ", name)
	fmt.Println("n5 = ", n5)

	n6, height := 6.9, 100.6
	fmt.Println("n6 = ", n6)
	fmt.Println("height = ", height)

	fmt.Println("n7 = ", n7)
	fmt.Println("n8 = ", n8)

	fmt.Println("n9 = ", n9)
	fmt.Println("n10 = ", n10)

}
