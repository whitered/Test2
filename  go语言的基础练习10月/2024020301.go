package main

import (
	"fmt"
	"strconv"
)

// 系统函数、内置函数
func main() {
	// 统计字符串的长度，按字节进行统计，在golang中，汉字是utf-8字符集，一个汉字3个字节
	str := "golang你好"
	fmt.Println(len(str))
	str_01 := "huanhuan"
	fmt.Println("str_01 = ",len(str_01))
	str_02 := "程序员"
	fmt.Println("str_02 = ",len(str_02))
	str_03 := "123$%^"
	fmt.Println("str_03 = ",len(str_03))

	// 对字符串进行遍历
	// fmt.Println("对数组进行遍历")
	// for i, value := range str {
	// 	fmt.Printf("索引为：%d, 具体的值为： %c \n", i, value)
	// }
	// fmt.Println()
	// fmt.Println("------------------------------------------------------------")
	// fmt.Println("对数组进行遍历 -- str_01")
	// for i, value : = range str_01 {
	// 	fmt.Printf("索引为：%d, 具体的值为： %c \n", i, value)
	// }
	// fmt.Println("------------------------------------------------------------")
	
	// 方式二：r:=[]rune(str)
	fmt.Println("------------------------------------------------------------")
	r := []rune(str_01)
	for i := 0; i < len(r); i++ {
		fmt.Printf("%c ", r[i])
	}

	fmt.Println()
	r01 := []rune(str_02)
	for i := 0; i < len(r01); i++ {
		fmt.Printf("%c ", r01[i])
	}

	fmt.Println()
	r03 := []rune(str_03)
	for i := 0; i < len(r03); i++ {
		fmt.Printf("%c ", r03[i])
	}
	fmt.Println()
	fmt.Println("------------------------------------------------------------")

	// 字符串转整数
	// n, err := strconv.Atoi("66")
	// fmt.Println(n, err)

	// 整数转字符串
	str = strconv.Itoa(6887)
	fmt.Println(str)

	// 查找子串是否在指定的字符串中  ??
	// count := string.Count("golangandjavaga", "ga")
	// fmt.Println(count)

	// 不区分大小写的字符串比较
	one := strings.EqualFold("hello", "HeLLo")
	fmt.Println(one)

}