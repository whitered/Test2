package main

import "fmt"

func main() {
	sum := cal(10, 20)
	fmt.Println(sum)

	var num3 int = 30
	var num4 int = 40
	sum1 := cal(num3, num4)
	fmt.Println(sum1)

	sum2, result := cal2(num3, num4)
	fmt.Println(sum2, result)

	var one1 int = 10
	var one2 int = 200
	fmt.Printf("交换前的两个数：num1 = %v, num2 = %v\n", one1, one2)
	exchangeNum(one1, one2)
	fmt.Printf("交换后的两个数：num1 = %v, num2 = %v\n", one1, one2)
}

func exchangeNum(one1 int, one2 int) {
	var t int
	t = one1
	one1 = one2
	one2 = t

}

func cal2(num3 int, num4 int) (int, int) {
	sum := num4 + num3
	reault := num4 - num3
	return sum, reault
}

func cal(i int, i2 int) int {
	var sum int = 0
	sum += i
	sum += i2
	return sum
}
