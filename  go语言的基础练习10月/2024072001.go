package main

import "fmt"

func main() {
	//定义字符类型的数据
	var c1 byte = 'a'
	fmt.Println("c1 = ", c1)

	var c2 byte = '6'
	fmt.Println("c2 = ", c2)

	var c3 byte = '('
	fmt.Println("c3 + 20  = ", c3+20)

	//字符类型，本质上就是一个整数，也可以直接参与运算，输出字符的时候，会将对应的码值做一个输出
	//字母，数字，标点等字符，底层就按照 ASCII 进行存储
	var c4 int = '中'
	fmt.Println("c4 = ", c4)
	fmt.Printf("c4对应的具体的字符为：%c\n", c4)
	//汉字字符，底层对应的是 Unicode 码值
	//对应的码值为:20013，byte 类型溢出，能存储的范围：可以用 int
	//总结：Golang 的字符对应的使用的是 UTF-8 编码（Unicode 是对应的字符集，UTF-8是 Unicode 的其中的一种编码方案）
	var c5 byte = 'A'
	//想显示对应的字符，必须采用格式化输出
	fmt.Println("c5 = ", c5)
	fmt.Printf("c5对应的具体的字符为：%c", c5)
	fmt.Println()

	var c6 string = "欢欢"
	fmt.Println("c6 = ", c6)
	//fmt.Printf("c6的值是：%t \n", c6)
	fmt.Println()

	//转义字符
	fmt.Println("aaa\nbbb")
	fmt.Println()
	fmt.Println("aaa\bbbb")
	fmt.Println()

	//光标回到本行的开头，后续输入就会替换原有的字符
	fmt.Println("光标回到本行的开头，后续输入就会替换原有的字符")
	fmt.Println("aaaaa\nbbb")
	fmt.Println()
	//制表符
	fmt.Println("aaaaaaaaaaaaa")
	fmt.Println("aaaaa\nbbbbb")
	fmt.Println("aaaaaaaa\nbbbbb")
	fmt.Println()
	fmt.Println("\"Golang\"")
	fmt.Println("\"")
	fmt.Println(` This is tset "" are you ok "" dddd `)
	fmt.Println()

	//测试布尔类型的数值
	var flag01 bool = true
	fmt.Println(flag01)

	var flag02 bool = false
	fmt.Println(flag02)

	var flag03 bool = 5 < 9
	fmt.Println(flag03)

	//定义一个字符串
	var s1 string = "你好全面拥抱Golang"
	fmt.Println("s1的内容是:", s1)
	//字符串是不可变的：指的是字符串一旦定义好，其中的字符的值不能改变
	var s2 string = "abc"
	fmt.Println("s2的值是：", s2)

	//字符串的拼接效果
	var s5 string = "abc " + "def "
	s5 += "hijk"
	fmt.Println(s5)
	fmt.Println()

	var s6 string = "abc " + "def " + "ghi " +
		"abc " + "def " + "ghi "
	fmt.Println(s6)

	fmt.Println()
	fmt.Println("--------------------------------------------------------------")
	var aa int
	var bb float32
	var cc float64
	var dd bool
	var ee string

	fmt.Println(aa)
	fmt.Println(bb)
	fmt.Println(cc)
	fmt.Println(dd)
	fmt.Print(ee)
	fmt.Println("--------------------------------------------------------------")

	//进行类型转换
	var n1 int = 100
	//fmt.Println(n1)
	//在这里自动转换不好使，比如显式转换
	var n2 float32 = float32(n1)
	fmt.Println(n2)
	fmt.Printf("%T", n1)
	fmt.Println()
	fmt.Printf("%T", n2)
	fmt.Println()
	var n3 int64 = 888888
	var n4 int8 = int8(n3)
	fmt.Println(n4)

	var n5 int32 = 12
	//一定要匹配 = 左右的数据类型
	var n6 int64 = int64(n5) + 30
	fmt.Println("n6 = ", n6)
	fmt.Printf("n5的值的类型是：%T \n", n5)
	fmt.Printf("n6的值的类型是：%T", n6)
	fmt.Println()

	var n7 int64 = 12
	var n8 int8 = int8(n7) + 11
	fmt.Printf("n8的值的类型是：%T \n", n8)
	fmt.Printf("n7的值的类型是：%T", n7)
	fmt.Println()

	var n11 int = 19
	//var n22 float32 = 4.78
	//var n33 bool = false
	//var n44 byte = 'a'

	var s11 string = fmt.Sprintf("%d", n11)
	fmt.Printf("s11对应的类型是：%T , s11 = %q \n", s11, s11)

}
