package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var num01 = 28
	// Printf函数的作用就是:格式化的,把num01的类型填充到%T的位置上
	fmt.Printf("num01的类型是: %T", num01)	// num01的类型是：int
	fmt.Println()

	fmt.Println(unsafe.Sizeof(num01))

	// 定义一个整数类型
	var num02 int8 = 120
	fmt.Println("num02 = ", num02)

	var num03 uint8 = 200
	fmt.Println("num03 = ", num03)

	var age byte = 18
	fmt.Println(age)

	fmt.Println("------------------------------------------------")
	// 定义浮点类型的数据：
	var flo01 float32 = 3.14
	fmt.Println("flo01 = ", flo01)

	// 可以表示正浮点数，也可以表示负的浮点数
	var flo02 float32 = -3.14
	fmt.Println("flo02 = ", flo02)

	// 浮点数可以用十进制表示形式，也可以用科学计数法表示形式， E大小写都可以的
	var  flo03 float32 = 314E-2
	fmt.Println("flo03 = ", flo03)
	var  flo04 float32 = 314E+2
	fmt.Println("flo04 = ", flo04)
	var  flo05 float32 = 314e+2
	fmt.Println("flo05 = ", flo05)
	var  flo06 float32 = 314e+2
	fmt.Println("flo06 = ", flo06)

	// 浮点数可能会有精度的损失，所以通常情况下，建议使用：float64
	var flo07 float32 = 256.00000916
	fmt.Println("flo07 = ", flo07)
	var flo08 float64 = 256.00000916
	fmt.Println("flo08 = ", flo08)

	// golang中默认的浮点数类型为：float64
	var flo09 = 3.17
	fmt.Println("flo09 = ", flo09)
	fmt.Println()
	fmt.Printf("flo09对应的默认的类型为: %T", flo09)

	fmt.Println()
	fmt.Println("---------------------------------------------")
	// 定义字符类型的数据：
	var by01 byte = 'a'
	fmt.Println("by01 = ", by01)  // 97
	var by02 byte = '6'
	fmt.Println("by02 = ", by02)  // 54
	var by03 byte = '('
	fmt.Println("by03 = ", by03)

	// 字符类型，本质上就是一个整数，也可以直接参与运算，输出字符的时候，会将对应的码值做一个输出
	// 字母，数字，标点等字符，底层是按照ASSII进行存储。

	var by04 int = '中'
	fmt.Println("by04 = ", by04)
	// 汉字字符，底层对应的是Unicode码值
	// 对应的码值为20013，byte类型溢出，能存储的范围：可以用int
	// 总结：Golang的字符对应的使用的是UTF-8编码（Unicode是对应的字符集，UTF-8是Unicode的其中的一种编码方案）

	var by05 byte = 'A'
	// 想显示对应的字符，必须采用格式化输出
	fmt.Printf("by05对应的具体的字符为: %c", by05)

	fmt.Println("\n---------------------------------------------")
	// 练习转义字符：
	// \n 换行
	fmt.Println("aaaa\nbbb")

	// \b 退格
	fmt.Println("aaaa\bbbb")

	// \r 光标回到本行的开头，后续输入就会替换原有的字符
	fmt.Println("aaaaa\rbbb")

	fmt.Println("aaaaaaaaaaaaaaaaaaaaaaaa")
	fmt.Println("aaaaa\tbbbbb")
	fmt.Println("aaaaaaaa\tbbbbb")

	// \" 
	fmt.Println(" \"Golang\"")

}