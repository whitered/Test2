package main

import (
	"fmt"
)

func main() {
	fmt.Println("This is a test!")
	// 定义字符类型的数据
	var c1 byte = 'a'
	fmt.Println(c1)
	var c11 byte = 'b'
	fmt.Println(c11)
	var c12 byte = 'A'
	fmt.Println(c12)
	var c13 byte = 'O'
	fmt.Println(c13)
	fmt.Printf("\t c13 = %c ", c13)

	// 参与运算
	fmt.Println()
	fmt.Println(c1 - 32 )
	var c21 byte = 'A'
	fmt.Println(c21 + ',' )
	fmt.Printf("A + , 的值是：%c", (c21 + ','))

	// 布尔类型
	fmt.Println("\n")
	fmt.Println("------------------------------")
	// 布尔类型的数值
	var flag01 bool = true
	fmt.Println(flag01)

	var flag02 bool = false
	fmt.Println(flag02)

	var flag03 bool = 5 < 9
	fmt.Println(flag03)

	var flag04 bool = 5 > 9
	fmt.Println(flag04)

	var flag05 bool = true != false
	fmt.Println(flag05)

	var huan int = 64
	var deng int = 10
	var shui bool = huan > deng
	fmt.Println(shui)

	var h int = 10
	var d int = 10
	var s bool = h == d
	fmt.Println(s)

}