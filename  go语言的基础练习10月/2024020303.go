package main

import (
	"fmt"
	// "strconv"
	// "time"
)

// 内置函数
func main() {
	// 定义一个字符串
	str := "golang"
	fmt.Println(len(str))
	str01 := "huanhuan"
	fmt.Println(len(str01))
	str02 := "denghong"
	fmt.Println(len(str02))
	str03 := "学好golang！能胜任一份工作"
	fmt.Println(len(str03))
	str04 := "学好"
	fmt.Println(len(str04))
	str05 := "！"
	fmt.Println(len(str05))
	fmt.Println()
	fmt.Println("------------------------------------------------------------")

	num := new(int)
	fmt.Printf("num的类型： %T \n", num)
	fmt.Printf("num的值是： %v \n", num)
	fmt.Printf("num的地址： %v \n", &num)
	fmt.Printf("num指针指向的值是： %v \n", *num)

	// 进行类型转换
	var n1 int = 100
	// var n2 float32 = n1
	fmt.Println("n1 = ", n1)
	fmt.Printf("n1的类型是：%T", n1)
	fmt.Println()
	var n2 float32 = float32(n1)
	fmt.Println("n2 = ", n2)

	// 高转低 溢出
	var n3 int64 = 888888
	fmt.Println("n3 = ", n3)
	var n4 int8 = int8(n3)
	fmt.Println("n4 = ", n4)

	// 一定要匹配=左右的数据类型
	var n5 int32 = 12
	var n6 int64 = int64(n5) + 30
	fmt.Println("n5 = ", n5)
	fmt.Println("n6 = ", n6)

	var n_01 int8 = 8
	var n_02 int16 = int16(n_01) + 123
	fmt.Println("n_01 = ", n_01)
	fmt.Println("n_02 = ", n_02)
}