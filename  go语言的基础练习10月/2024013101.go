package main

import (
	"fmt"
)

func main() {
	fmt.Println("This is a test!")     
	
	// 练习转义字符 换行
	fmt.Println("aaa\nbbb")
	fmt.Println("1111\n2222\n3333\n4444")
	fmt.Println("这是一个例子\n新的案例到来")
	fmt.Println("专科升本科\n研究生双学位")
	// \b 退格
	fmt.Println("aaa\bbbb")
	fmt.Println("python\bjava\bphp\bgolang")
	fmt.Println("这是一个例子\b新的案例到来")
	fmt.Println("1111\b2222\b3333\b4444")

	// \r 光标回到本行的开头，后续输入就会替换原有的字符
	fmt.Println()
	fmt.Println("1111\r2222\r3333\r4444")
	fmt.Println("aaaaa\rbbb")
	fmt.Println("huanhuanhuanhuan\rdenghong")
	fmt.Println("欢欢欢欢\r乐乐")

	// \t 制表符
	fmt.Println()
	fmt.Println("aaaaaaaaaaaaaa")
	fmt.Println("aaaaa\tbbbbb")
	fmt.Println("aaaaaaaa\tbbbbb")
	fmt.Println("aaaabbbb\teeeeffff")

	// \"
	fmt.Println()
	fmt.Println("\"golang\"")
	fmt.Println("\"huanhuan\"")
	fmt.Println("\"Are you ok!\"")
}