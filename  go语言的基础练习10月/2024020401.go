package main

import (
	"fmt"
	"unsafe"
)

// 内置函数
func main() {
	// var num1 int8 = 230
	var num1 int16 = 230
	fmt.Println("num1 = ", num1)

	// 超出边界报错
	// var num2 uint8 = -20
	var num2 int8 = -20
	fmt.Println("num2 = ", num2)
	// var num2_01 uint8 = 1000
	// fmt.Println("num2_01 = ", num2_01)
	var num2_02 uint16 = 10000
	fmt.Println("num2_02 = ", num2_02)

	// 其它的整数类型
	var num3 = 28
	fmt.Printf("num3的类型是：%T", num3)
	fmt.Println()
	// 变量占用的字节数
	fmt.Println("num3 = ", unsafe.Sizeof(num3))
	fmt.Println()
	var num3_01 = -10220.12
	fmt.Printf("num3_01的类型是：%T", num3_01)
	fmt.Println()
	// 变量占用的字节数
	fmt.Println("num3_01 = ", unsafe.Sizeof(num3_01))

	// 在golang中使用变量的原则：保小不保大
	var age byte = 18
	fmt.Println("age = ", age)
	fmt.Println("------------------------------------------------------------")

	// 定义浮点类型的数据
	var float_01 float32 = 3.14
	fmt.Println("float_01 = ", float_01)
	// 浮点数的正负
	var float_02 float32 = -3.14
	fmt.Println("float_02 = ", float_02)
	// 十进制， 科学计数法； E大小写都可以
	var float_03 float32 = 314E-2
	fmt.Println("float_03 = ", float_03)
	var float_04 float32 = 314E+2
	fmt.Println("float_04 = ", float_04)
	var float_05 float32 = 314e+2
	fmt.Println("小e float_05 = ", float_05)
	var float_06 float32 = 314e+2
	fmt.Println("小e float_06 = ", float_06)

	// 浮点数可能会有

	fmt.Println()

	fmt.Println("------------------------------------------------------------")

}