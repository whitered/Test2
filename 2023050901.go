package main

import (
	"fmt"
)

func main() {
	var num1 int = 10
	fmt.Println(num1)
	var num2 int = (10+20)%3 + 7 - 7
	fmt.Println(num2)

	var num3 int = 10
	num3 += 20
	fmt.Println("num3 = ", num3)

	// 交换2个数的值并输出结果
	var a int = 8
	var b int = 4
	fmt.Printf("a = %v, b = %v", a, b)
	fmt.Println()

	var t int
	t = a
	a = b
	b = t
	fmt.Printf("a = %v, b = %v", a, b)
	fmt.Println()

	// 测试案例01，交换
	var one bool = true
	var two bool = false
	fmt.Printf("one = %v, two = %v", one, two)
	fmt.Println()
	// 交换，中间值
	var middle bool
	middle = one
	one = two
	two = middle
	fmt.Printf("one = %v, two = %v", one, two)
	fmt.Println()

	// 测试案例02，交换
	var ming string = "ming"
	var anhei string = "anhei"
	fmt.Printf("ming = %v, anhei = %v", ming, anhei)
	fmt.Println()
	// 交换，中间值
	var midd string
	midd = ming
	ming = anhei
	anhei = midd
	fmt.Printf("ming = %v, anhei = %v", ming, anhei)

}
