package main

import (
	"fmt"
)

func main() {
	// 加减乘除 正数 相加 字符串拼接
	var n1 int = +10
	fmt.Println(n1)
	var n2 int = 4 + 7
	fmt.Println(n2)

	var s1 string = "abc" + "def"
	fmt.Println(s1)

	// 数字为: 0
	var zero int = 0
	fmt.Println(zero)
	// 数字为负数 -
	var fu01 int = -123
	fmt.Println("fu01 = ", fu01)

	// 数字相加 正数 加 负数
	var number01 int = 12 + (-2)
	fmt.Println("number01 = ", number01)
	var number02 float64 = 3.14 + 3.21
	fmt.Println("number02 = ", number02)
	var number03 float64 = 5.14 + 12
	fmt.Println("number03 = ", number03)

	// 拼接
	var huanhuan string = "shuiQiang" + " ai " + "huanhuan"
	fmt.Println("huanhuan = ", huanhuan)
	var shui string = "shui" + " yun " + "tian"
	fmt.Println("shui = ", shui)

	// 新的内容 除法
	fmt.Println()
	fmt.Println(10 / 3)
	fmt.Println(10.0 / 3)
	fmt.Println(10 / 3.0)

	// 取模   %
	fmt.Println()
	fmt.Println(10 % 3)
	fmt.Println(-10 % 3)
	fmt.Println(10 % -3)
	fmt.Println(-10 % 3)

	fmt.Println()

	var a int = 10
	a++
	fmt.Println(a)
	a--
	fmt.Println(a)

}
