package main

import (
	"fmt"
)

// 自定义函数：功能交换两个数
func exchangeNum(num1 int, num2 int) {
	var t int
	t = num1
	num1 = num2
	num2 = t
	// return num1,num2
	fmt.Printf("交换之后: num1 = %v, num2 = %v \n", num1, num2)
}

func main() {
	var num1 int = 10
	var num2 int = 20
	fmt.Printf("交换之前: num1 = %v, num2 = %v \n", num1, num2)
	exchangeNum(num1, num2)
	// fmt.Printf("交换之后: num1 = %v, num2 = %v \n", num1, num2)

}
