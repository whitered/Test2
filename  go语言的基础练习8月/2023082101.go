package main

import (
	"fmt"
)

func main() {
	var arr [2][3]int16

	// 赋值
	arr[0][1] = 47
	arr[0][0] = 82
	arr[1][1] = 25
	fmt.Println(arr)

	// 初始化
	var arr1 [3][3]int = [3][3]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	fmt.Println(arr1)
	fmt.Println()

	var arr02 [2][2]int8
	fmt.Println(arr02)
	// 赋值
	arr02[0][0] = 25
	arr02[0][1] = 35
	arr02[1][0] = 24
	arr02[1][1] = 34

	fmt.Println(arr02)
	// 内存地址
	fmt.Printf("arr02[0]的内存地址: %p", &arr02[0])
	fmt.Println()
	fmt.Printf("arr02[0][0]的内存地址: %p", &arr02[0][0])
	fmt.Println()
	fmt.Printf("arr02[0][1]的内存地址: %p", &arr02[0][1])
	fmt.Println()
	fmt.Printf("arr02[1][0]的内存地址: %p", &arr02[1][0])
	fmt.Println()

	// 案例02
	fmt.Println("案例--02")
	var one [3][2]int = [3][2]int{{11, 111}, {22, 222}, {33, 333}}
	fmt.Println(one)

	// 改变值
	one[0][0] = 20
	one[0][1] = 22
	fmt.Println(one)
    

}
