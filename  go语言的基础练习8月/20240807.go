package main

import (
	"fmt"
	"unsafe"
)

func main() {
	//num的定义、类型、空间
	var num uint8 = 230
	fmt.Println(num)
	fmt.Printf("num的类型是：%T\n", num)
	fmt.Println("num占的字节数是：", unsafe.Sizeof(num))
	fmt.Println()

	//指针
	var age = 36
	//& + 变量 就可以获取这个变量的内存的地址
	fmt.Println("age的内容地址：", &age)
	//fmt.Println(age)

	var newAge = &age
	fmt.Println(newAge)
	fmt.Println("newAge存储空间的地址为：", &newAge)
	//newAge 的值为：36
	fmt.Printf("newAge 的值为：%v", *newAge)
	fmt.Println()

	//理解1
	var name = "huanhuan"
	//&name 是一个地址 P_name 是指针变量的名字；
	var P_name = &name
	fmt.Println(P_name)
	fmt.Println(&P_name)
	fmt.Printf("P_name 的值为：%v", *P_name)
	fmt.Println()

	fmt.Println("指针理解")
	//var aa int = 35
	//var bb int32
	//var cc float32
	var (
		aa int     = 35
		bb int32   = 40
		cc float32 = 3.14
	)

	fmt.Printf("第1行 -aa 变量类型为 = %T \n", aa)
	fmt.Printf("第2行 -bb 变量类型为 = %T \n", bb)
	fmt.Printf("第3行 -cc 变量类型为 = %T \n", cc)

	//指针的测试
	prtr := &aa
	fmt.Printf("a 的值为 %d \n", aa)
	fmt.Printf("*prtr 为 %d \n", *prtr)

	cpro := &cc
	fmt.Printf("cc 的值为 %v \n", cc)
	fmt.Printf("*cpro 为 %v \n", *cpro)

}
