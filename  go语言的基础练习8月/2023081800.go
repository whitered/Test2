package main

import (
	"fmt"
)

func main() {
	// 定义二维数组
	var arr [2][3]int16
	fmt.Println(arr)

	fmt.Printf("arr的地址是: %p", &arr)
	fmt.Println()
	fmt.Printf("arr[0]的地址是: %p", &arr[0])
	fmt.Println()
	fmt.Printf("arr[0][0]的地址是: %p", &arr[0][0])
}
