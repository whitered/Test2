package main

import "fmt"

func main() {
	fmt.Println("位运算符")
	var a uint = 60
	var b uint = 13
	var c uint = 0

	c = a & b
	fmt.Printf("第 1 行 & - c 的值为： %d\n", c)

	c = a | b
	fmt.Printf("第 2 行 | - c 的值为： %d\n", c)

	c = a ^ b
	fmt.Printf("第 3 行 ^ - c 的值为： %d\n", c)

	c = a >> 2
	fmt.Printf("第 4 行 >> - c 的值为： %d\n", c)

	c = a << 2
	fmt.Printf("第 5 行 << - c 的值为： %d\n", c)
	// 不当成重点内容操作
	fmt.Println()
	fmt.Println("赋值运算符")
	var aa int = 21
	var cc int

	cc = aa
	fmt.Printf("第 1 行 = - cc 的值为： %d\n", cc)

	cc += aa
	fmt.Printf("第 2 行 += - cc 的值为： %d\n", cc)

	cc -= aa
	fmt.Printf("第 3 行 -= - cc 的值为： %d\n", cc)

	cc *= aa
	fmt.Printf("第 4 行 *= - cc 的值为： %d\n", cc)

	cc /= aa
	fmt.Printf("第 5 行 /= - cc 的值为： %d\n", cc)

	cc = 200

	cc <<= 2
	fmt.Printf("第 6 行 <<= - cc 的值为： %d\n", cc)

	cc >>= 2
	fmt.Printf("第 7 行 >>= - cc 的值为： %d\n", cc)

	cc &= 2
	fmt.Printf("第 8 行 &= - cc 的值为： %d\n", cc)

	cc ^= 2
	fmt.Printf("第 9 行 ^= - cc 的值为： %d\n", cc)

	cc |= 2
	fmt.Printf("第 10 行 |= - cc 的值为： %d\n", cc)

}
