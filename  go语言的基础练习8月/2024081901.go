package main

import "fmt"

const (
	i = 1 << iota
	j = 3 << iota
	k
	l
)

func main() {
	fmt.Println("Hello World")
	fmt.Println("i = ", i)
	fmt.Println("j = ", j)
	fmt.Println("k = ", k)
	fmt.Println("l = ", l)

	fmt.Println("运算符的使用")
	var (
		a int = 21
		b int = 10
		//b int = 21
		//c int
	)
	//c = a + b // +
	//fmt.Printf("第1行- 加 - C =  %d\n", c)
	//c = a - b // -
	//fmt.Printf("第2行- 减 - C =  %d\n", c)
	//c = a * b // *
	//fmt.Printf("第3行- 乘 - C =  %d\n", c)
	//c = a / b // /
	//fmt.Printf("第4行- 除 - C =  %d\n", c)
	//c = a % b // % 取余
	//fmt.Printf("第5行- 取余 - C =  %d\n", c)
	//fmt.Println("a = ", a)
	//a++ // ++
	//fmt.Printf("第6行- 除 - a =  %d\n", a)
	//fmt.Println("a = ", a)
	//a-- // --
	//fmt.Printf("第7行- 除 - a =  %d\n", a)

	fmt.Println("关系运算符 == ！= > < >= <= ")
	if a == b {
		fmt.Printf("第1行 a 等于 b \n")
	} else {
		fmt.Printf("第1行 a 不等于 b \n")
	}

	if a != b {
		fmt.Printf("第2行 a 不等于 b \n")
	} else {
		fmt.Printf("第2行 a 等于 b \n")
	}

	if a > b {
		fmt.Printf("第3行 a 大于 b \n")
	} else {
		fmt.Printf("第3行 a 不大于 b \n")
	}

	fmt.Println("换参数 i j ")
	var (
		i float64 = 36.55
		j float64 = 35.65
	)
	if i <= j {
		fmt.Printf("第4行 i <= j \n")
	}

	if i >= j {
		fmt.Printf("第5行 i >= j \n")
	}

}
