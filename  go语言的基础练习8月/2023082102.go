package main

import (
	"fmt"
)

func main() {
	// 定义二维数组
	var arr [3][3]int = [3][3]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	fmt.Println(arr)
	fmt.Println("-----------二维数组的循环------------")
	// 方式一，普通的双重循环 for循环
	for i := 0; i < len(arr); i++ {
		for j := 0; j < len(arr[i]); j++ {
			fmt.Print(arr[i][j], "\t")
		}
		fmt.Println()
	}

	fmt.Println("-----------------------")
	// 方式二,for - range 循环
	for key, value := range arr {
		for k, v := range value {
			fmt.Printf("arr[%v][%v]=%v\t", key, k, v)
		}
		fmt.Println()
	}

}
