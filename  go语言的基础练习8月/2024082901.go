package main

import (
	"fmt"
)

func main() {
	//多维数组
	var courseInfo [3][4]string
	courseInfo[0] = [4]string{"go", "1h", "bobby0", "go体系课"}
	courseInfo[1] = [4]string{"grpc", "1h", "bobby1", "grpc体系课"}
	courseInfo[2] = [4]string{"gin", "1h", "bobby2", "gin体系课"}

	fmt.Println(courseInfo)
	fmt.Println(len(courseInfo))
	//fmt.Println(courseInfo[2])
	for i := 0; i < len(courseInfo); i++ {
		for j := 0; j < len(courseInfo[i]); j++ {
			fmt.Print(courseInfo[i][j] + " ")
		}
		fmt.Println()
	}
	//多维数组 2 class
	fmt.Println("多维数组 2")
	var lesson [3][4]string
	lesson[0] = [4]string{"english", "10h", "钱老师", "大学英语（二）"}
	lesson[1] = [4]string{"高等数学", "20h", "韩老师", "高等数学（本）"}
	lesson[2] = [4]string{"线性代数", "30h", "李老师", "大学线性代数"}
	//fmt.Println(lesson)
	//fmt.Println(len(lesson))
	for i := 0; i < len(lesson); i++ {
		for j := 0; j < len(lesson[i]); j++ {
			fmt.Print(lesson[i][j] + " ")
		}
		fmt.Println()
	}

	//多维数组 3 class
	fmt.Println("多维数组 3")
	var color [4][4]string
	color[0] = [4]string{"white", "白", "10", "霜白"}
	color[1] = [4]string{"red", "红", "10", "中国红"}
	color[2] = [4]string{"green", "绿", "10", "草原绿"}
	color[3] = [4]string{"blue", "蓝", "10", "天蓝"}
	for i := 0; i < len(color); i++ {
		for j := 0; j < len(color[i]); j++ {
			fmt.Print(color[i][j] + " ")
		}
		fmt.Println()
	}

}
