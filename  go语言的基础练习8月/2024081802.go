package main

import "fmt"

func main() {
	var a bool = true
	var b bool = false

	if a && b {
		fmt.Println("第一行 - 条件为：true", "true")
	}

	if a || b {
		fmt.Println("第二行 - 条件为：true")
	}

	//修改 a 和 b 的值
	a = false
	b = true
	if a && b {
		fmt.Println("第三行 - 条件为：true")
	} else {
		fmt.Println("第三行 - 条件为：false")
	}
	if a || b {
		fmt.Println("第四行 - 条件为：true")
	}

	if !(a && b) {
		fmt.Printf("第五行 - 条件为：true")
	}
	fmt.Println()
	fmt.Println("---------------------------")

	var (
		shui bool = true
		huan bool = false
	)
	if shui && huan {
		fmt.Println(true)
	} else {
		fmt.Println(false)
	}

	if huan || shui {
		fmt.Println(true)
	} else {
		fmt.Println(false)
	}

	//shui01 := true

	//与逻辑： && 两个数值/表达式只要有一侧是 false ，结果一定为 false
	//也叫短路与， 只要第一个数值/表达式的结果是 false ， 那么后面的表达式就不用运算了。
	//直接结果就是 false --> 提高运算效率
	fmt.Println("---------与- 和 -and-------")
	fmt.Println("0-0", true && true)
	fmt.Println(true && false)
	fmt.Println(false && true)
	fmt.Println(false && false)
	//fmt.Printf("%v", shui01)

	//或 逻辑： || ， 两个数值，表达式只要有一侧是：true 。 结果一定为： true
	//短路或，只要第一个数值/表达式的结果是 true ， 那么后面的表达式就不用运算了。
	fmt.Println("---------或-or--------")
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(false || true)
	fmt.Println(false || false)

	//非 逻辑， 去相反的结果
	fmt.Println("---------非--not-------")
	fmt.Println(!true)
	fmt.Println(!false)

	fmt.Println("---------与 - 和 --------")
	// 和
	red := true
	green := false
	fmt.Println(red && green)

	fmt.Println(red || false)
	fmt.Println("red || green = ", red || green)

	fmt.Println(!red)
	fmt.Println(!green)

	fmt.Println("--------name--------")
	name := red
	if name == red {
		fmt.Println("这是：true")
	} else {
		fmt.Println("这是：false")
	}

	switch name {
	case name:
		fmt.Println("这是：true")
	default:
		fmt.Println("这是：false")
	}
}
