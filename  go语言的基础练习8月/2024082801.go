package main

import "fmt"

func main() {
	//运算符优先级
	var a int = 20
	var b int = 10
	var c int = 15
	var d int = 5
	var e int

	e = (a + b) * c / d
	fmt.Printf("(a + b) * c / d 的值为： %d\n", e)

	e = ((a + b) * c) / d
	fmt.Printf("((a + b) * c) / d 的值为： %d\n", e)

	e = (a + b) * (c / d)
	fmt.Printf("(a + b) * (c / d) 的值为： %d\n", e)

	e = a + (b*c)/d
	fmt.Printf("a + (b * c) / d 的值为： %d\n", e)

	//if 条件判断
	fmt.Println()

	//实现功能：如果库存小于30，提示：库存不足
	var count int = 100
	if count < 30 {
		fmt.Println("库存不足，获取订单失败！")
	}
	fmt.Println("库存充足，获取订单成功！")

	var chengJi int = 75
	if chengJi > 60 {
		fmt.Println("恭喜你！ 本次考核及格！！！")
	}

	var yunDong int = 120
	if yunDong > 110 {
		fmt.Println("完成本次任务！ 希望您再接再厉！")
	}

	var newOne int = 120
	if newOne > 100 {
		fmt.Println("完成")
	}
	fmt.Println("----------------------------------")
	//双分支
	var count01 int = 100
	if count01 < 30 {
		fmt.Println("数量低于30，需要准备进货")
	} else {
		fmt.Printf("数量充足,还有：%d \n", count01)
	}

	//区分是否成年
	var age int = 30
	if age < 18 {
		fmt.Println("未成年，需要学校的保护和教育！")
	} else {
		var age_B int
		fmt.Printf("已经进成年:")
		age_B = age - 18
		fmt.Println(age_B)
	}

	//区别颜色
	var color string = "blue"
	if color == "red" {
		fmt.Println("颜色是red")
	} else {
		fmt.Printf("颜色是：%s", color)
	}

}
