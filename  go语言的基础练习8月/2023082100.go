package main

import (
	"fmt"
)

func main() {
	// 定义二维数组
	var arr [2][3]int16
	fmt.Println(arr)

	// 输出 数组的第一位数的地址
	fmt.Printf("arr的地址是: %p", &arr)
	fmt.Println()
	fmt.Printf("arr[0]的地址是: %p", &arr[0])
	fmt.Println()
	fmt.Printf("arr[0][0]的地址是: %p", &arr[0][0])
	fmt.Println()
	fmt.Println("----------------------------------")

	// 数组第二位数的内存地址
	fmt.Printf("arr[1]的地址是: %p", &arr[1])
	fmt.Println()
	fmt.Printf("arr[1][0]的地址是: %p", &arr[1][0])
	fmt.Println()
	fmt.Println("----------------------------------")

	// 给二维数组赋值
	arr[0][1] = 47
	arr[0][0] = 82
	arr[1][1] = 25

	fmt.Println(arr)

}
