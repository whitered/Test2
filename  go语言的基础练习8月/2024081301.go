package main

import "fmt"

func main() {
	fmt.Println("Hello World")
	var num = 36
	fmt.Println(num)

	var ptr = &num
	fmt.Println(ptr)
	fmt.Println(&num)
	fmt.Println()

	// %d 表示整数型数字， %s 表示字符串
	var stockcode = 123
	//fmt.Println(stockcode)
	var enddate = "2024-12-31"
	//fmt.Println(enddate)
	var url = "Code=%d &endDate= %s"
	//fmt.Println(url)
	var target_url = fmt.Sprintf(url, stockcode, enddate)
	fmt.Println(target_url)

	//来源于 runoob 的实例
	//var a string = "huanHuan"
	//fmt.Println(a)

	var b, c = 1, 2
	fmt.Println(b, c)

	fmt.Println()
	var blue = "蓝色"
	fmt.Println(blue)

	var one, two, three, four = "1.明确目标", "2、技能", "3、稳定", "4、提升能力"
	fmt.Println(one, two, three, four)
	fmt.Println()

	//声明一个变量并初始化
	var a = "runoob"
	fmt.Println(a)

	//没有初始化，输出默认值，int 默认值为：0
	var b_01 int
	fmt.Println("b_01 = ", b_01)

	//bool的默认值
	var bl01 bool
	fmt.Println("bl01 = ", bl01)

	var a1 *int
	//var a1 []int
	//var a1 map[string]int
	//var a1 chan int
	//var a1 func(string) int
	//var a1 error
	fmt.Println("a1 = ", &a1)
	fmt.Println()

	//默认值展示
	var ii_01 int
	var ff_01 float64
	var bb_01 bool
	var ss_01 string
	fmt.Printf("%v \n %v \n %v \n  %q\n", ii_01, ff_01, bb_01, ss_01)

	//float32() 的默认值是多少
	var fl32 float32
	fmt.Println("fl32的默认值：", fl32)

}
