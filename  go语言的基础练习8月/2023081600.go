package main

import (
	"fmt"
)

func main() {
	i := 1
	for i <= 5 {
		fmt.Println("你好 Golang")
		i++
	}
	fmt.Println()

	for i < 10 {
		fmt.Println("欢欢，我想和你一起好好的！！")
		i++
	}
	fmt.Println("i结束")

	j := 0
	for j <= 5 {
		fmt.Println("你好 Golang 这是J")
		j++

	}
	fmt.Println()
	for j < 10 {
		fmt.Println("加油这得靠自己！")
		j++

	}
	fmt.Println()
	for j < 12 {
		fmt.Println("加油这得靠自己！这是12")
		j++
		fmt.Println("j = ", j)
	}

}
