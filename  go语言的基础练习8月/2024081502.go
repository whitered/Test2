package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello World")
	fmt.Println("---------------------------------")

	//name := "imooc go体系课"
	//nameRune := []rune(name)
	//fmt.Println(len(name))
	//fmt.Println()
	//for index, value := range nane {
	//	fmt.Println(index, value)
	//}

	//for index, _ := range nane {
	//	fmt.Println(nane[index])
	//}

	//for i := 0; i < len(nameRune); i++ {
	//	print(string(nameRune[i]))
	//}

	//for _, v := range nameRune {
	//	fmt.Printf("%c", v)
	//}

	//print(len(name))
	//fmt.Println(nameRune)

	//round := 0
	//for {
	//	time.Sleep(1 * time.Second)
	//	round++
	//	//fmt.Println(round)
	//	//if round > 10 {
	//	//	break
	//	//}
	//	if round != 5 {
	//		fmt.Println(round)
	//	}
	//	if round > 10 {
	//		break
	//	}
	//}

	//for i := 1; i <= 5; i++ {
	//	fmt.Printf("这是第%d行 \n", i)
	//	for j := 0; j < 4; j++ {
	//		//fmt.Println(i, j)
	//		if j == 3 {
	//			break
	//		}
	//		fmt.Println(i, j)
	//	}
	//
	//}

	//goto 语句可以让我们的代码跳到指定的代码块中运行， 所以说很少用
	//go 语句的goto 语句可以实现程序的跳转，goto语句使用场景最多的是程序的错误处理。
	// 错误的统一处理
	//	for i := 0; i < 6; i++ {
	//		for j := 0; j < 5; j++ {
	//			if j == 2 {
	//				goto over
	//			}
	//			fmt.Print(i, j, "\n")
	//		}
	//		fmt.Println()
	//	}
	//over:
	//	fmt.Println("over")

	fmt.Println("循环")
	//day := "星期一"
	//switch day {
	//case day:
	//
	//}

}
