package main

import (
	"fmt"
)

const (
	a = "abc"
	b = len(a)
	//c = unsafe.Sizeof(a)
)

func main() {
	//遍历，处理第几行
	//for y := 1; y <= 9; y++ {
	//	for x := 1; x <= y; x++ {
	//		fmt.Printf("%d * %d = %d \t", x, y, x*y)
	//	}
	//	fmt.Println()
	//}

	// for 循环还有一种用法: for range , 主要是对：字符串、数组、切片、map、channel
	name := "imooc go"
	//for index, value := range name {
	//	//fmt.Println(index, value)
	//	fmt.Printf("%d, %c \r\n", index, value)
	//}

	for _, value := range name {
		//fmt.Println(index, value)
		fmt.Printf("%d, \r\n", value)
	}

	const LENGTH = 10
	const WIDTH = 5
	var area int

	//const a, b, c = 1, false, "str"
	//求面积
	area = LENGTH * WIDTH
	fmt.Printf("面积为：%d \n", area)
	fmt.Println()
	//fmt.Println(a, b, c)

	const (
		Unknow = 0
		Female = 1
		Male   = 2
	)

	fmt.Println(Female, Male, Unknow)

	fmt.Println(a, b, c)

	fmt.Println("--------------------------------------------------")
	//const (
	//	aa = iota
	//	bb = iota
	//	cc = iota
	//	dd = iota
	//)
	//const (
	//	aa = iota
	//	bb
	//	cc
	//	dd
	//)
	//fmt.Println(aa, bb, cc, dd)

	const (
		aa = iota
		bb
		cc
		dd = "huanHuan"
		ee
		ff = 100
		gg
		hh = iota
		ii
	)

	fmt.Println(aa, bb, cc)
	fmt.Println(ee, dd)
	fmt.Println(ff, gg)
	fmt.Println(hh, ii)

}
