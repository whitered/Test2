package main

import "fmt"

var x, y int
var (
	a int
	b bool
)

var c, d int = 1, 2
var e, f = 123, "hello"

func main() {
	//根据值自行判定变量类型
	var d = true
	fmt.Println(d)

	var nun = 36
	fmt.Println(nun)
	fmt.Printf("nun的类型是：%T", nun)
	fmt.Println()

	var shui = "shuiHuman"
	fmt.Printf("shui的类型： %T", shui)
	fmt.Println()

	intVal := 1018
	fmt.Println(intVal)
	fmt.Printf("intVal 的类型： %T", intVal)
	fmt.Println()

	muBao := "golang"
	fmt.Println(muBao)
	fmt.Printf("muBao 的类型： %T", muBao)
	fmt.Println()

	huan := true
	fmt.Println(huan)
	fmt.Printf("huan 的类型： %T", huan)
	fmt.Println()

	nunFlo := 3.1515926
	fmt.Println(nunFlo)
	fmt.Printf("nunFlo 的类型： %T", nunFlo)
	fmt.Println("\n")

	fmt.Println("多变量声明")
	fmt.Println("***************************")
	var (
		one01 int = 12
		one02 int = 24
		one03 int = 36
	)
	fmt.Println(one01)
	fmt.Println(one02)
	fmt.Println(one03)

	fmt.Println("案例-1")
	var vname1, vname2, vname3 int
	vname1, vname2, vname3 = 10, 20, 30
	fmt.Println(vname1, vname2, vname3)

	fmt.Println("案例-2")
	fmt.Println("x=", x, " \t y=", y)

	fmt.Println(a, b)
	fmt.Println(c, d, e, f)

	var ce_01, ce_02, ce_03 byte
	ce_01, ce_02, ce_03 = 'a', 'b', 'c'
	fmt.Println(ce_01, ce_02, ce_03)

	var ch1, ch2, ch3 float32
	ch1, ch2, ch3 = 3.14, 3.14, 3.14
	fmt.Println(ch1, ch2, ch3)
	//fmt.Printf("ch1 %T", ch1)

	red, white := "red", "white"
	fmt.Println(red, white)
	fmt.Printf("red = %T\n", red)
	fmt.Printf("white = %T\n", white)

	blue, blue_1 := 36, "霜"
	fmt.Println(blue, blue_1)
	fmt.Printf("blue = %T\n", blue)
	fmt.Printf("blue_1 = %T\n", blue_1)

	fmt.Println()
	fmt.Println("***************************")
	var (
		yellow   = 123
		yellow01 = 6.45
		yellow02 = "hello"
	)

	fmt.Println(yellow, yellow01, yellow02)

	//从 1 + 100 的总和
	var sum int
	for i := 1; i <= 100; i++ {
		sum += i
	}
	fmt.Println(sum)

}
