package main

import "fmt"

func main() {
	//定义一个数组
	var arr01 = [3]int{3, 6, 9}
	fmt.Printf("数组的类型为：  %T \n", arr01)

	var arr02 = [...]string{"3", "java", "two", "javascript"}
	fmt.Printf("数组的类型为：  %T \n", arr02)

	var arr03 = [...]string{0: "golang", 6: "java", 9: "mysql"}
	fmt.Printf("数组的类型为：  %T \n", arr03)
	fmt.Println(len(arr03))
	for v, l := range arr03 {
		fmt.Println(v, "---->", l)
	}

	var arr3 = [3]int{3, 6, 9}
	//test1(arr3)
	test1(&arr3)
	fmt.Println(arr3)
	fmt.Println()

	var arr04 = [...]string{1: "java", 0: "go", 2: "mysql", 3: "javascript"}
	demo(&arr04)
	fmt.Println(arr04)

	fmt.Println("--------新建的二维数组--------")
	var arr [3][3]int = [3][3]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	fmt.Println(arr)
	fmt.Println("-------------------------------")
	for i := 0; i < len(arr); i++ {
		fmt.Print("[")
		//fmt.Println(arr[i])
		for j := 0; j < len(arr[i]); j++ {
			fmt.Print(arr[i][j], " ")
		}
		fmt.Println("]")
	}

	fmt.Println("方式 2 ")
	for key, value := range arr {
		//fmt.Println(key, value)
		for k, v := range value {
			fmt.Printf("arr[%v][%v] = %v \t", key, k, v)
		}
		fmt.Println()
	}

}

func demo(arr04 *[4]string) {
	(*arr04)[3] = "js"
	(*arr04)[0] = "html"
}

func test1(arr3 *[3]int) {
	(*arr3)[0] = 7
}

//func test1(arr3 [3]int) {
//	arr3[0] = 7
//	fmt.Println(arr3)
//}
