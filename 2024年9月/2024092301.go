package main

import (
	"fmt"
	"math"
)

func main() {
	// 变量复习
	name, age := "nonhuman", 30
	fmt.Println(name, age)

	a, b := 20, 30
	fmt.Printf("a = %d, b = %d \n", a, b)
	b, c := 40, 50
	fmt.Printf("b = %d, c = %d \n", b, c)
	b, c = 100, 200
	fmt.Printf("b = %d, c = %d \n", b, c)
	fmt.Println()

	aa, bb := 30, 40
	fmt.Printf("aa = %d, bb = %d \n", aa, bb)
	aa, bb = 60, 80
	fmt.Printf("aa = %d, bb = %d \n", aa, bb)

	aa01, bb01 := 100.12, 200.13
	c = int(math.Min(aa01, bb01))
	fmt.Println(c)

	//bool 布尔值
	bool11 := true
	bool00 := false
	fmt.Println("bool11 =", bool11, "\t bool00 =", bool00)
	c01 := bool11 && bool00
	fmt.Println("c01 =", c01)
	d01 := bool11 || bool00
	fmt.Println("d01 =", d01)

	var aa03 int = 89
	bb02 := 95
	fmt.Println("value of a is ", aa03, " and bb02 is ", bb02)

}
