package main

import "fmt"

func main() {
	//多维数组
	a := [3][2]string{
		{"lion", "tiger"},
		{"cat", "dog"},
		{"pigeon", "peacock"},
	}

	printarray(a)
	var b [3][2]string
	b[0][0] = "apple"
	b[0][1] = "samsung"
	b[1][0] = "microsoft"
	b[1][1] = "google"
	b[2][0] = "AT&T"
	b[2][1] = "T-Mobile"
	fmt.Printf("\n")
	printarray(b)

	a1 := [5]int{76, 77, 78, 79, 80}
	var b1 = a1[1:4]
	fmt.Println(b1)

	//创建切片
	fmt.Println("\n 创建切片 ")
	c := []int{6, 7, 8, 9, 10}
	fmt.Println(c)

	//切片的修改
	darr := [...]int{57, 89, 90, 82, 100, 78, 67, 69, 59}
	dslice := darr[2:5]
	fmt.Println("array before", darr)
	for i := range dslice {
		dslice[i]++
	}
	fmt.Println("array after ", darr)

	//多个切片共用数组
	fmt.Println(" \n 多个切片共用数组")
	numa := [3]int{78, 79, 80}
	nums1 := numa[:]
	nums2 := numa[:]
	fmt.Println("array before", numa)
	nums1[0] = 100
	fmt.Println("array after modification to slice nums1", numa)
	nums2[1] = 101
	fmt.Println("array after modification to slice nums2", numa)
	fmt.Println("numa = ", numa)

	//切片的长度和容量
	fmt.Println(" \n 切片的长度和容量")
	fruitArray := [...]string{"apple", "orange", "grape", "mango", "water melon", "pine apple", "chinkoo"}
	fruitSlice := fruitArray[1:3]
	fmt.Printf("length of slice %d capacity %d", len(fruitSlice), cap(fruitSlice))
	fruitSlice = fruitSlice[:cap(fruitSlice)]
	fmt.Println()
	fmt.Println("After re-slicing length is ", len(fruitSlice), "and capacity is ", cap(fruitSlice))

	// 使用 make 创建一个切片
	fmt.Println(" \n 使用 make 创建一个切片")
	i := make([]int, 5, 5)
	fmt.Println(i)
}

func printarray(a [3][2]string) {
	for _, v1 := range a {
		for _, v2 := range v1 {
			fmt.Printf("%s ", v2)
		}
		fmt.Printf("\n")
	}
}
