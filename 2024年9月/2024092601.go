package main

import "fmt"

func main() {
	//切片
	var intarr [6]int = [6]int{1, 2, 3, 4, 5, 6}
	var slice []int = intarr[1:4]
	fmt.Println("slice = ", slice)
	fmt.Println("slice的长度 = ", len(slice))

	slice = append(slice, 88, 50)
	fmt.Println("slice = ", slice)
	fmt.Println("slice的长度 = ", len(slice))

	//切片的追加
	slice2 := []int{99, 44}
	slice = append(slice, slice2...)
	fmt.Println("slice = ", slice)
	fmt.Println("slice的长度 = ", len(slice))
	fmt.Println()

	//切片的拷贝
	fmt.Println("--------- 切片的拷贝 ---------")
	//定义切片 a
	var a []int = []int{11, 22, 33}
	//定义切片 b
	var b []int = make([]int, 10)
	//拷贝 a 复制到 b
	copy(b, a)
	fmt.Println(b)
	fmt.Printf("a = %v, a 的长度 = %v", a, len(a))
	fmt.Println()
	fmt.Printf("b = %v, b 的长度 = %v", b, len(b))
	fmt.Println("\n-----------------02-------------------")

	//案例01
	//声明一个未指定大小的数组来定义切片
	var numbers []int
	//fmt.Printf("numbers = %v\n", numbers)
	printSlice(numbers)

	if numbers == nil {
		fmt.Printf("切片是空的")
	}
}

func printSlice(x []int) {
	fmt.Printf("len=%d , cap= %d, slice=%v \n", len(x), cap(x), x)
}
