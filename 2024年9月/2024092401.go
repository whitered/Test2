package main

import "fmt"

func main() {
	// 函数
	price, no := 90, 6
	totalPrice := calculateBill(price, no)
	fmt.Println(totalPrice)

	//area, perimeter := rectProps(10.8, 5.6)
	//fmt.Printf("area: %f, perimeter: %f\n", area, perimeter)

	area, _ := rectProps(10.8, 5.6)
	fmt.Printf("area: %f\n", area)

	var a int = 100
	var b int = 200
	var ret int
	ret = max(a, b)
	fmt.Printf("最大值是：%d\n", ret)

}

func rectProps(length, width float64) (float64, float64) {
	var area = length * width
	var perimeter = (length + width) * 2
	return area, perimeter
}

//func rectProps(length, width float64) (float64, float64) {
//	var area = length * width
//	var perimeter = (length + width)
//	return area, perimeter
//}

func calculateBill(price, no int) int {
	var totalPrice = price * no
	return totalPrice
}
