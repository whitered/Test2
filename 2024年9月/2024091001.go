package main

import "fmt"

func main() {
	courses01 := [...]string{"golang", "java", "go", "ruby"}

	courses02 := [...]string{"golang", "java", "go", "ruby"}

	if len(courses01) == len(courses02) {
		fmt.Println("数组长度相等")
	}
	if courses01 == courses02 {
		fmt.Println("数组相等")
	}

	//数组的入门
	//实现的功能：给出五个学生的成绩，求出成绩的总和，平均数：
	//score1 := 95
	//score2 := 91
	//score3 := 89
	//score4 := 45
	//score5 := 56
	//sum := score1 + score2 + score3 + score4 + score5
	//avg := sum / 5
	//fmt.Println(sum, avg)

	//升级为数组
	var scores [5]int
	scores[0] = 95
	scores[1] = 91
	scores[2] = 89
	scores[3] = 45
	scores[4] = 56
	//求和，定义一个变量接收和
	var sum int
	for i := 0; i < len(scores); i++ {
		sum += scores[i]
	}
	fmt.Println(sum)
	//平均数
	avg := sum / len(scores)
	fmt.Println(avg)
	fmt.Printf("成绩的总和为:%v， 平均数为：%v \n", sum, avg)

	fmt.Println("-------------内存分析----------------")
	var arr [3]int16
	//获取数组的长度
	fmt.Println(len(arr))
	//打印数组
	fmt.Println(arr)
	//证明 arr 中存储的是地址值
	fmt.Printf("arr的地址为： %p \n", &arr)
	//第一个空间的地址
	fmt.Printf("arr的地址为： %p \n", &arr[0])
	//第 2 个空间的地址
	fmt.Printf("arr的地址为： %p \n", &arr[1])
	//第 3 个空间的地址
	fmt.Printf("arr的地址为： %p \n", &arr[2])

	var scores2 [5]int
	for i := 0; i < len(scores2); i++ {
		fmt.Printf("请输入第 %d 个学生的成绩: ", i+1)
		fmt.Scanln(&scores2[i])
	}
	for i := 0; i < len(scores2); i++ {
		fmt.Printf("第 %d 个学生的成绩为：%d \n", i+1, scores2[i])
	}
}
