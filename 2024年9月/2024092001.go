package main

import "fmt"

func main() {
	//多维切片
	pls := [][]string{
		{"C", "C++"},
		{"JavaScript"},
		{"go", "Rust"},
	}

	for _, v1 := range pls {
		for _, v2 := range v1 {
			fmt.Printf("%s \t", v2)
		}
		fmt.Println()
	}

	//内存优化
	countriesNeeded := countries()
	fmt.Println(countriesNeeded)
}

func countries() []string {
	countries := []string{"USA", "Singapore", "Germany", "India", "Australia"}
	neededCountries := countries[:len(countries)-2]
	countriesCpy := make([]string, len(neededCountries))
	copy(countriesCpy, neededCountries)
	return countriesCpy
}
