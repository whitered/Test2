package main

import "fmt"

func main() {
	//数组的初始化
	//第 1 种
	var arr1 [3]int = [3]int{3, 6, 9}
	fmt.Println(arr1)
	var array01 [4]int = [4]int{1, 2, 3, 4}
	fmt.Println(array01)
	var col01 [5]int = [5]int{1, 2, 3, 4, 5}
	fmt.Println(col01)
	var col02 [4]int = [4]int{36, 35, 20, 12}
	fmt.Println(col02)
	fmt.Println()

	//第 2 种
	var arr2 = [3]int{1, 4, 7}
	fmt.Println(arr2)
	var new01 = [2]string{"java", "golang"}
	fmt.Println(new01)
	var this01 = [6]string{"html", "css", "javaScrip", "mysql", "php"}
	fmt.Println(this01)
	var blue01 = [5]string{"一心一意", "心无旁骛", "熟能生巧", "怒发冲冠", "胆小如鼠"}
	fmt.Println(blue01)
	var three01 = [...]string{"洗衣机", "电风扇", "空调", "冰箱", "彩电", "电饭煲", "微波炉"}
	fmt.Println(len(three01))
	fmt.Println(three01)
	fmt.Println()

	//第 3 种
	fmt.Println("第 3 种")
	var arr3 = [...]int{4, 5, 6, 7}
	fmt.Println(arr3)
	var dou01 = [...]float64{3.14, 5.4, 63.11}
	fmt.Println("dou01 = ", len(dou01))
	fmt.Println(dou01)
	var two02 = [...]int64{136, 2365, 336}
	fmt.Println(two02)
	var three02 = []uint{10, 20, 30}
	fmt.Println(three02)
	fmt.Println()

	//第 4 种
	fmt.Println("第 4 种")
	var arr4 = [...]int{1: 66, 2: 22, 3: 14}
	fmt.Println(arr4)

	var who01 = [...]string{0: "java", 1: "javaScrip", 2: "mysql", 3: "php"}
	fmt.Println(who01)
	var who02 = [...]float64{3: 3.14, 0: 0.12, 2: 114.11}
	fmt.Println(who02)
	fmt.Println()
	var who03 = []bool{true, false, true}
	fmt.Println(who03)

}
