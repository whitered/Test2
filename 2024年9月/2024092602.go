package main

import "fmt"

func main() {
	//切片截取 创建切片
	numbers := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	//var numberss []int
	printSlice(numbers)

	//打印原始切片
	fmt.Println("numbers == ", numbers)
	//打印切片的片段
	fmt.Println("numbers[1:4] ==", numbers[1:4])
	//默认下限为： 0
	fmt.Println("numbers[:3] ==", numbers[:3])
	//默认上限为：len(s)
	fmt.Println("numbers[4:] ==", numbers[4:])

	fmt.Println("--------------")
	numbers1 := make([]int, 0, 5)
	printSlice(numbers1)

	//打印子切片从索引 0（包含） 到索引 2（不包含）
	fmt.Println("索引 0（包含） 到索引 2（不包含）")
	numbers2 := numbers[:2]
	printSlice(numbers2)

	//打印子切片从索引 2（包含） 到索引 5（不包含）
	fmt.Println("索引 2（包含） 到索引 5（不包含）")
	numbers3 := numbers[2:5]
	printSlice(numbers3)

}

func printSlice(x []int) {
	fmt.Printf("len=%d cap=%d slice=%v\n", len(x), cap(x), x)
}
