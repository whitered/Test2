package main

import "fmt"

func changeLocal(num [5]int) {
	num[0] = 55
	fmt.Println("inside function", num)
}

func main() {
	//切片
	//	定义数组
	var intArr = [6]int{1, 2, 3, 4, 5, 6}
	fmt.Println(intArr)
	slice := intArr[1:3]
	fmt.Println(slice)
	fmt.Println("len:", len(slice))
	fmt.Println("cap:", cap(slice))
	fmt.Println("len:", len(intArr))
	// 切片元素的个数
	fmt.Println("slice元素的个数:", len(slice))
	//获取切片的容量：容量可以动态变化
	fmt.Println("slice的容量:", cap(slice))

	//make函数定义切片
	fmt.Println("\n方式2， make")
	slice02 := make([]int, 3, 5)
	fmt.Println(slice02)
	fmt.Println("slice02的长度:", len(slice02))
	fmt.Println("slice02的容量:", cap(slice02))
	slice02[0] = 66
	slice02[1] = 88
	slice02[2] = 89
	fmt.Println(slice02)
	//遍历
	fmt.Println(" ----- 遍历 for 循环 -----")
	for i := 0; i < len(slice02); i++ {
		fmt.Printf("slice02[%v] = %v \t", i, slice02[i])
	}
	fmt.Println("\n遍历 for 循环 end")

	//方式3，直接就指定具体的数组
	fmt.Println("\n方式3")
	slice03 := []int{1, 2, 3}
	fmt.Println(slice03)
	fmt.Println("slice03的长度:", len(slice03))
	fmt.Println("slice03的容量:", cap(slice03))

	fmt.Println("\n go语言中文网")
	var a [3]int
	fmt.Println(a)
	a[0] = 12
	a[1] = 78
	a[2] = 50
	fmt.Println(a)

	a1 := []int{12, 78, 50}
	fmt.Println(a1)

	a2 := [3]int{12}
	fmt.Println(a2)

	a3 := [...]int{12, 78, 50}
	fmt.Println(a3)

	a4 := [...]float64{67.7, 89.8, 21, 78}
	sum := float64(0)
	for i, v := range a4 {
		fmt.Printf("%d the element of a is %.2f\n", i, v)
		sum += v
	}
	fmt.Println("\n sum of all elements of a ", sum)

	//数字是值类型
	a5 := [...]string{"USA", "China", "India", "Germany", "France"}
	a6 := a5
	a6[0] = "Singapore"
	fmt.Println("a5 is ", a5)
	fmt.Println("a6 is ", a6)

	//参数传递
	fmt.Println("\n 参数传递-案例")
	num := [...]int{5, 6, 7, 8, 8}
	fmt.Println("before passing to function ", num)
	changeLocal(num)
	fmt.Println("after passing to function ", num)
	//数组的长度
	fmt.Println("num 数组的长度： ", len(num))
	fmt.Println("a5 数组的长度： ", len(a5))
	fmt.Println("slice03 数组的长度： ", len(slice03))

	//使用 range 迭代数组
	a7 := [...]float64{67.7, 89.8, 21, 78}
	//for i := 0; i < len(a7); i++ {
	//	//fmt.Printf("a[%d] is %.2f \n", i, a7[i])
	//	fmt.Printf("%d th element of a is %.2f \n", i, a7[i])
	//}
	sum = float64(0)
	for i, v := range a7 {
		fmt.Printf("%d th element of a is %.2f \n", i, a7[i])
		sum += v
	}
	fmt.Println("\n sum of all element of a", sum)
}
