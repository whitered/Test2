package main

import (
	"fmt"
)

func main() {
	//多维数组
	var coursesInfo [3][4]string
	coursesInfo[0] = [4]string{"go", "120min", "zhu", "这是主要的技能要用最多的时间"}
	coursesInfo[1] = [4]string{"MySQL", "30min", "hu", "数据库很重要"}
	coursesInfo[2] = [4]string{"Linux", "30min", "fu", "服务器系统非常重要"}
	fmt.Println(len(coursesInfo))

	//for i := 0; i < len(coursesInfo); i++ {
	//	fmt.Println(coursesInfo[i])
	//}

	for i := 0; i < len(coursesInfo); i++ {
		for j := 0; j < len(coursesInfo[i]); j++ {
			fmt.Print(coursesInfo[i][j] + " ")
		}
		fmt.Println()
	}

	//fmt.Println()
	//for _, row := range coursesInfo {
	//	for _, course := range row {
	//		fmt.Print(course + " ")
	//	}
	//	fmt.Println()
	//}

	fmt.Println()
	for _, row := range coursesInfo {
		fmt.Println(row)
	}

	fmt.Println()
	courses01 := [3]string{"go", "grpc", "gin"}
	for _, value := range courses01 {
		fmt.Println(value)
	}

	fmt.Println("没有值的数组")
	courses02 := [3]string{2: "gin"}
	for _, value := range courses02 {
		fmt.Println(value)
	}
	fmt.Println()

}
