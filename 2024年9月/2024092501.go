package main

import "fmt"

func main() {
	//切片
	var intarr [6]int = [6]int{1, 2, 3, 4, 5, 6}
	slice := intarr[1:3]
	//输出数组
	fmt.Println("intarr =", intarr)
	//输出切片
	fmt.Println("slice =", slice)

	//切片元素个数
	fmt.Println("slice 个数 =", len(slice))
	//获取切片的容量：容量可以动态变化
	fmt.Println("slice 容量 =", cap(slice))
	//遍历
	fmt.Println("\n--------遍历--------")
	for i := 0; i < len(slice); i++ {
		fmt.Printf("slice[%v] = %v \t", i, slice[i])
	}

	//方式二
	fmt.Println("\n \n定义切片的方式二")
	slice2 := make([]int, 4, 20)
	fmt.Println("slice2 =", slice2)
	slice2[0] = 66
	slice2[1] = 88

	//切片元素个数
	fmt.Println("slice2 个数 =", len(slice2))
	//获取切片的容量：容量可以动态变化
	fmt.Println("slice2 容量 =", cap(slice2))
	fmt.Println("slice2 =", slice2)
	fmt.Println()

	//方式三
	fmt.Println("定义切片的方式-3")
	slice3 := []int{1, 4, 7}
	fmt.Println("slice3 =", slice3)
	//切片元素个数
	fmt.Println("slice3 个数 =", len(slice3))
	//获取切片的容量：容量可以动态变化
	fmt.Println("slice3 容量 =", cap(slice3))

}
