package main

import (
	"fmt"
	"strconv"
)

func main() {
	var s01 string = "true"
	var b bool
	b, _ = strconv.ParseBool(s01)
	fmt.Printf("b的类型是: %T, b = %v", b, b)

	fmt.Println()
	var s02 string = "19"
	var num01 int64
	num01, _ = strconv.ParseInt(s02, 10, 64)
	fmt.Printf("num01的类型是: %T, num1 = %v \n", num01, num01)

}
