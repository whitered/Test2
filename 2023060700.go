package main

import (
	"fmt"
)

// 自定义函数：两个数相加
func cal(num1 int, num2 int) int {
	var sum int = 0
	sum += num1
	sum += num2
	return sum
}

func main() {
	// 功能：10+ 20
	// 调用函数
	/*
		var num1 int = 10
		var num2 int = 20

		// 定义一个数接收和
		var sum int = 0
		sum += num1
		sum += num2
		fmt.Println(sum)
	*/

	sum := cal(10, 20)
	fmt.Println(sum)
	fmt.Println()

	// 新的案例1 30 50
	// var num3 int = 30
	// var num4 int = 50

	// 求和
	/*
		var sum_all int = 0
		sum_all += num3
		sum_all += num4
		fmt.Println(sum_all)
	*/

	// 求和 --- 2

	sum_all := cal(30, 50)
	fmt.Println(sum_all)

}
