package main

import (
	"fmt"
)

// 全局变量，定义在函数外的变量
var outside int = 100

// 全局变量，浮点数，测试
var outside01 = 3.14

// 全局变量声明赋值名字
var name = "white"

func main() {
	var age int // 变量声明
	age = 18    // 变量赋值
	// 变量的使用
	// fmt.Println(age)
	fmt.Println("age = ", age)

	// 声明和赋值可以合成一句
	var age2 int = 35
	// 输出
	fmt.Println("age2 = ", age2)

	// 声明一个工作的工龄,并赋值
	var age_work_year int = 5
	fmt.Println("工龄是:", age_work_year)

	// 声明一台电脑是寿命，并赋值6年
	var computer_year int = 6
	// 输出时间
	fmt.Println("电脑的使用时间是：", computer_year)

	// 输出空行,把代码隔开
	fmt.Println()

	// 全局变量，outside 输出
	fmt.Println("全局变量输出：", outside)
	// 全局变量，浮点数，输出，没有表明数据类型
	fmt.Println("outside01 = ", outside01)
	// 全局变量输出名字
	fmt.Println("name = ", name)

}
