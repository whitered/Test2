package main

import (
	"fmt"
)

func main() {
	// 求1-100的和
	var sum int = 0 // sum 接收和的值
	for i := 1; i <= 100; i++ {
		sum += i
		// fmt.Println(i + sum = sum)
		// fmt.Println("%v"+"%v = %v", i, sum-i, sum)
		fmt.Println(sum)
		if sum >= 30 {
			break
		}
	}
	fmt.Println("-----------认真思考------------")

	// var one int = 0
	// for i := 0; i <= 20; i++ {
	// 	one = one + i
	// 	fmt.Println(one)
	// 	if one > 90 {
	// 		break
	// 	}
	// }
	// fmt.Println("one大于90停止")

	// var two int = 0
	// for a := 0; a <= 10; a = a + 2 {
	// 	two = two + a
	// 	fmt.Println(two)
	// 	if two >= 12 {
	// 		break
	// 	}
	// }
	// fmt.Println("two的值大于12停止")

	// fmt.Println("练习3")
	// var three int = 0
	// for n := 1; n <= 10; n++ {
	// 	three = three + n
	// 	fmt.Println(three)
	// 	if three > 10 {
	// 		break
	// 	}
	// }
	// fmt.Println("测试")

	// 双重循环
	for i := 1; i <= 7; i++ {
		// fmt.Println(i)
		for j := 1; j <= 8; j++ {
			// fmt.Println("j = ", j)
			fmt.Printf("i: %v, j: %v \n", i, j)
			if i == 2 && j == 2 {
				break
			}
			// if i == 3 && j == 3 {
			// 	break
			// }
			// if i == 4 && j == 4 {
			// 	break
			// 	// break结束内循环，继续外层循环
			// }
		}
	}

	fmt.Println("双重循环2")
	for n := 1; n <= 7; n++ {
		// fmt.Println(n)
		for m := 1; m <= 10; m++ {
			// fmt.Println(n, m)
		}
	}

}
