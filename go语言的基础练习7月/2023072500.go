package main

import "fmt"

func main() {
	// 双重循环
	// label:
	// for i := 1; i <= 5; i++ {
	// 	// fmt.Println(i)
	// 	for j := 1; j < 5; j++ {
	// 		// fmt.Println(i, j)
	// 		if i == 2 && j == 2 {
	// 			continue label
	// 		}
	// 		fmt.Printf("i=%v, j=%v \n", i, j)
	// 	}
	// 	fmt.Println()
	// }
	// fmt.Println("------执行完成------")

	// 双重循环 M N
	// fmt.Println("双重循环M / N")
	// for m := 0; m < 3; m++ {
	// 	// fmt.Println(m)
	// 	for n := 1; n <= 6; n = n + 2 {
	// 		// fmt.Println(m, n)
	// 		if n == 1 {
	// 			fmt.Println(m, n)
	// 		} else {
	// 			fmt.Println(" ", n)
	// 		}
	// 	}
	// 	fmt.Println()
	// }
	// fmt.Println("--------ok--------")

	fmt.Println("label标签")
label:
	for g := 0; g <= 10; g = g + 3 {
		// fmt.Println(g)
		for q := 3; q < 20; q = q + 5 {
			if g == 3 && q == 13 {
				fmt.Println("---g == 3,  q == 13--- \n")
				continue label

			}
			fmt.Printf("g=%v, q=%v \n", g, q)
			// fmt.Println(q)
		}
		fmt.Println()
	}

}
