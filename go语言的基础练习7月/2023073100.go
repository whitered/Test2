package main

import (
	"fmt"
)

//	func cal(num1 int, num2 int) int {
//		var sum int = 0
//		sum += num1
//		sum += num2
//		return sum
//	}
//
// 方法二
// func cal(num1 int, num2 int) {
// 	var sum int = 0
// 	sum += num1
// 	sum += num2
// 	fmt.Println(sum)
// }

// 如果返回值类型就一个的话，那么()是可以省略不写
func cal(num1 int, num2 int) int {
	var sum int = 0
	sum += num1
	sum += num2
	return sum
}

// 三个数相加
func newNum(num1 int, num2 int, num3 int) int {
	var oneNum int = 0
	oneNum += num1
	oneNum += num2
	oneNum += num3
	return oneNum
}

func main() {
	// 功能：10+20
	// 调用函数
	sum := cal(40, 20)
	fmt.Println(sum)

	oneNum := newNum(1, 2, 30)
	fmt.Println(oneNum)

	// 新的三个数相加
	twoNum := newNum(11, 2, 30)
	fmt.Println(twoNum)

	twoAdd := cal(10, 2)
	fmt.Println("twoAdd = ", twoAdd)

}
