package main

import (
	"fmt"
)

func main() {
	// 双重循环
	for i := 1; i <= 5; i++ {
		for j := 2; j <= 4; j++ {
			if j == 2 {
				fmt.Println("i = ", i, "j = ", j)
			} else {
				fmt.Println("      ", "j = ", j)
			}
			// if i == i {
			// 	continue
			// }
		}
		fmt.Println()

	}
}
