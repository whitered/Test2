package main

import (
	"fmt"
)

func main() {
	// 功能：输出1-100中被6整除的数
	// fmt.Println("方式一： ")
	// for i := 1; i <= 100; i++ {
	// 	if i%6 == 0 {
	// 		fmt.Println(i)
	// 	}
	// 	// fmt.Println("i = ", i)
	// }

	// fmt.Println("案例2")
	// for i := 0; i <= 100; i++ {
	// 	// fmt.Println("i = ", i)
	// 	if i%5 == 0 {
	// 		fmt.Println("i = ", i)
	// 	}
	// }

	// fmt.Println("案例3")
	// for i := 10; i < 100; i++ {
	// 	// fmt.Println("i = ", i)
	// 	if i%8 == 0 {
	// 		fmt.Println("i = ", i)
	// 	}
	// }

	// fmt.Println("方式二")
	// for i := 1; i <= 50; i++ {
	// 	// fmt.Println("i = ", i)
	// 	if i%5 != 0 {
	// 		continue
	// 	}
	// 	fmt.Println("i = ", i)
	// }

	// fmt.Println("continue_01")
	// for i := 10; i <= 50; i++ {
	// 	// fmt.Println(i)
	// 	if i%10 == 0 {
	// 		continue
	// 	}
	// 	fmt.Println("i = ", i)
	// }

	// fmt.Println("continue_02")
	// for i := 2; i < 100; i = i + 2 {
	// 	// fmt.Println("i = ", i)
	// 	if i%10 != 0 {
	// 		continue
	// 	}
	// 	fmt.Println("i = ", i)
	// }

	fmt.Println("continue_03")
	for i := 10; i <= 50; i = i + 5 {
		// fmt.Println("i = ", i)
		if i%2 != 0 {
			continue
		}
		fmt.Println("i = ", i)
	}
}
