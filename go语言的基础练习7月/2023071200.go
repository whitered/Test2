package main

import (
	"fmt"
	"strconv"
)

func main() {
	var str string = "true"
	var bool01 bool
	bool01, _ = strconv.ParseBool(str)
	fmt.Printf("bool01的类型是: %T, bool01 = %v \n", bool01, bool01)

	// string ---> int64
	var str02 string = "19"
	var num01 int64
	num01, _ = strconv.ParseInt(str02, 10, 64)
	fmt.Printf("num01的类型是: %T , num1 = %v \n", num01, num01)

}
