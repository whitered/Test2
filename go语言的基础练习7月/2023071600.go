package main

import (
	"fmt"
)

func main() {
	var count int = 20
	/*
		    // 单分支
			if count < 30 {
				fmt.Println("数量不足30")
			}
	*/

	/*
		if count := 20; count < 30 {
			fmt.Println("数量不足30!!")
		}
	*/

	// 双分支，一定会二选一其中一个分支。
	if count < 30 {
		fmt.Println("库存不足")
	} else {
		fmt.Println("数量充足")
	}

	var bool01 int = 220
	if bool01 > 300 {
		fmt.Println("正确")
	} else {
		fmt.Println("false")
	}
	fmt.Println()

	var one int = 23
	if one <= 25 {
		fmt.Println("one太小了")
	} else {
		fmt.Println("one大于25")
	}

	var two int = 18
	if two <= 18 {
		fmt.Println("你还没有成年！")
	} else {
		fmt.Println("您成年了")
	}
	fmt.Println("是否成年")

	var age int = 22
	if age >= 22 {
		fmt.Println("你是男的，已经满足结婚年龄！")
		fmt.Println("你是女的，也已经很大了，还在读书啦！")
	} else {
		fmt.Println("年龄低于22岁，男的满足结婚年龄！")
		fmt.Println("女的也行！")
	}
	fmt.Println()

}
