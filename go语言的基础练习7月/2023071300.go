package main

import (
	"fmt"
)

func main() {
	// 通过指针改变指向值
	var num int = 10
	fmt.Println("num = ", num)

	var ptr *int = &num
	*ptr = 20
	fmt.Println()
	fmt.Println("num = ", num)

	// 算数运算符
	fmt.Println("---------------------------------")
	var (
		n1    int    = +10
		n2    int    = 4 + 8
		str01 string = "abc " + "def"
	)

	fmt.Println("n1 = ", n1)
	fmt.Println("n2 = ", n2)
	fmt.Println("str01 = ", str01)

	fmt.Println("-------除-----------")
	fmt.Println("10 / 3 = ", 10/3)
	fmt.Println("10.0 / 3 = ", 10.0/3)

	fmt.Println("-------% 取模 -------")
	fmt.Println("10 % 3 = ", 10%3)
	fmt.Println("10.0 % 3 = ", 10%3)
	fmt.Println("-10 % 3 = ", -10%3)
	fmt.Println("10 % -3 = ", 10%-3)
	fmt.Println("-10 % -3 = ", -10%-3)

	// 自增操作
	fmt.Println()
	var (
		one int  = 10
		two int8 = 12
	)
	one++
	fmt.Println("one++ = ", one)
	one--
	fmt.Println("one-- = ", one)
	two--
	fmt.Println("two-- = ", two)
    // ++ 自增 加1操作， --自减，减1操作
    // ++ ， -- 只能单纯使用，不能参与到运算中去，并且只能写在前面

}
