package main

import (
	"fmt"
)

func main() {
	// 定义变量1-5
	var i1 int = 1
	var i2 int = 2
	var i3 int = 3
	var i4 int = 4
	var i5 int = 5

	// 求和 先定义一个变量，用来接收这个和
	var sum int = 0
	sum += i1
	sum += i2
	sum += i3
	sum += i4
	sum += i5

	// 输出结果
	fmt.Println("sum = ", sum)
	// 缺点：变量太多，很冗余

	fmt.Println("改进的方案二： ")
	// 定义变量
	var ii int = 1

	// 求和，定义一个变量，用来接收这个和
	var sum01 int = 0
	sum01 += ii
	ii++
	sum01 += ii
	ii++
	sum01 += ii
	ii++
	sum01 += ii
	ii++
	sum01 += ii
	ii++
	fmt.Println("sum01 = ", sum01)
	// 重复的代码套多了

	var sum02 int = 0
	for n := 1; n <= 5; n++ {
		sum02 += n
	}
	fmt.Println("sum02 = ", sum02)

	var sum03 int = 0
	for m := 1; m <= 15; m = m + 2 {
		sum03 += m
	}
	fmt.Println("sum03 = ", sum03)
	fmt.Println()

	fmt.Println("循环的细节 01 ")
	// 变量的初始化
	j := 1
	// 条件表达式
	for j <= 5 {
		// 循环体
		fmt.Println("加油！ 一定要成功，相信自己，努力就会有回报！")
		// 迭代
		j++
	}
	fmt.Println()

	fmt.Println("死循环")
	// for {
	// 	fmt.Println("不练习就不会进步！")
	// }
    // 遇到死循环，在控制台中结束死循环；ctrl +C
}
