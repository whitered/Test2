package main

import (
	"fmt"
)

func main() {
	var unm01 int = 10
	fmt.Println(unm01)

	var num02 int = (10+20)%3 + 3 - 7
	fmt.Println("num02 = ", num02)

	var num03 int = 10
	num03 += 20
	fmt.Println("num03 = ", num03)

	// 交换两个数的值，并输出结果
	fmt.Println()
	var (
		a int = 1
		b int = 0
	)

	fmt.Printf("a = %v, b = %v", a, b)
	fmt.Println()
	// 引入中间变量 t
	var t int
	t = a
	a = b
	b = t
	fmt.Printf("a = %v, b = %v \n", a, b)

	// 交换zero 和 one
	var (
		n  string = "zero"
		m  string = "one"
		mn string
	)
	fmt.Printf("n = %v, m = %v \n", n, m)
	fmt.Println(mn)
	mn = n
	n = m
	m = mn
	fmt.Printf("n = %v, m = %v \n", n, m)

	// 交换小红与小明的水果，小红=苹果 小明 = 香蕉
	var (
		x_hong string = "apple"
		x_ming string = "banana"
		middle string
	)
	fmt.Printf("x_hong的水果是: %v, x_ming的水果是: %v, \n", x_hong, x_ming)
	fmt.Println(middle)
	middle = x_hong
	x_hong = x_ming
	x_ming = middle
	fmt.Printf("x_hong的水果是: %v, x_ming的水果是: %v, \n", x_hong, x_ming)

}
