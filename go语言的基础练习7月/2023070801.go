package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var (
		num1 float32 = 3.14
		num2 float32 = -3.14
		num3 float32 = 314e-2
		num4 float32 = 314e+2
		num5 float64 = 314e+2
		// num6 float64 = 314e+2
		num6  float32 = 256.00000000000000916
		num7  float64 = 256.00000000000000916
		num8  float64 = 256.000015
		flo00 float32 = 256.000015
	)

	fmt.Println("num1 = ", num1)
	fmt.Println("num2 = ", num2)
	fmt.Println("num3 = ", num3)
	fmt.Println("num4 = ", num4)
	fmt.Println("num5 = ", num5)
	// fmt.Println("num6 = ", num6)
	fmt.Println("num6 = ", num6)
	fmt.Println("num7 = ", num7)
	fmt.Printf("num8对应的默认类型为: %T \n", num8)
	fmt.Println(unsafe.Sizeof(num6))
	fmt.Println("num8 = ", num8)

	fmt.Println("flo00 = ", flo00, "\nflo00占用存储空间", unsafe.Sizeof(flo00))
	fmt.Printf("flo00的默认类型是: %T", flo00)
	fmt.Println()

	fmt.Println("看得到风景的窗户")
	var (
		c1 byte = 'a'
		c2 byte = '6'
		c3 byte = '('
	)

	fmt.Println("c1 = ", c1)
	fmt.Printf("c1的默认类型: %T", c1)
	fmt.Println()
	fmt.Println("c2 = ", c2)
	fmt.Printf("c2的默认类型: %T \n", c2)
	fmt.Println("c3 = ", c3)
	// \n 换行
	fmt.Println("aaa\nbbb")
	// \b 退格
	fmt.Println("aaa\rbbb")
	// \t 制表符
	fmt.Println("aaaaa\tbbbbb")
	fmt.Println("aaaaaaaaaa\tbbbbb")

	// \"
	fmt.Println("\" Golang \"")
	var (
		flag01 bool = true
		flag02 bool = false
		flag03 bool = 5 > 2
	)

	fmt.Println("flag01 = ", flag01)
	fmt.Println("flag02 = ", flag02)
	fmt.Println("flag03 = ", flag03)
}
