package main

import (
	"fmt"
)

func main() {
	// 与逻辑：&& 2个数值 / 表达式只要有一侧是false,结果一定为false
	// 也叫做短路与：只要第一个数值/表达式的结果是false,那么后面的表达式等就不用运算了，直接结果就是false
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(false && true)
	fmt.Println(false && false)
	fmt.Println()

	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(false || true)
	fmt.Println(false || false)
	fmt.Println()

	// 非逻辑：取相反的结果
	fmt.Println(!true)
	fmt.Println(!false)
}
