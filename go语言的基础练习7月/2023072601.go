package main

import (
	"fmt"
	"strconv"
)

func main() {
	// 统计字符串的长度，按字节进行统计
	var str00 string = "今年目标"
	fmt.Println(len(str00))

	str01 := "过年拿壹万给老婆大人随便花"
	fmt.Println(len(str01))

	var (
		str02 string = "maiche"
		str03 string = "golang! 加油"
	)
	fmt.Println(len(str02))
	fmt.Println(len(str03))

	fmt.Println()
	fmt.Println("字符串遍历")
	// for-range
	// for i, str00 := range str00 {
	// 	fmt.Printf("索引为： %d, 具体的值为： %c \n", i, str00)
	// }

	// for i, str01 := range str03 {
	// 	fmt.Printf("索引为： %d, 具体的值为： %c \n", i, str01)
	// }

	// r:=[]rune(str)
	// r := []rune(str01)
	// for i := 0; i < len(r); i++ {
	// 	fmt.Printf("%c \n", r[i])
	// }

	// 3.字符串转整数
	// n, err := strconv.Atoi("66")
	// fmt.Println(n, err)
	// num01, _ := strconv.Atoi("666")
	// fmt.Println(num01)

	// 4.整数转字符串
	// str05 = strconv.Itoa(6887)
	// fmt.Println(str05)
	str11 := strconv.Itoa(88)
	fmt.Println(str11)
	fmt.Printf("str11的类型是: %T", str11)

}
