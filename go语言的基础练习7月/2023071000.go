package main

import (
	"fmt"
	"strconv"
	"unsafe"
)

func main() {
	var (
		n1 int     = 19
		n2 float32 = 4.12
		n3 bool    = false
		n4 byte    = 'B'
	)

	var str01 string = fmt.Sprintf("%d", n1)
	fmt.Printf("str01对应的类型是: %T , str01 = %q \n", str01, str01)

	// 浮点数
	// fmt.Sprintf("%f", n2)
	var str02 string = fmt.Sprintf("%f", n2)
	fmt.Printf("str02对应的类型是: %T , str02 = %q", str02, str02)
	fmt.Println()

	// bool类型
	var str03 string = fmt.Sprintf("%t", n3)
	fmt.Printf("str03对应的类型是: %T , str03 = %q \n", str03, str03)

	// 字符串
	var str4 string = fmt.Sprintf("%c", n4)
	fmt.Printf("str4对应的类型是: %T, s4 = %q \n", str4, str4)

	// strconv
	fmt.Println()
	fmt.Println("--------------------------")
	var (
		num_01 int     = 18
		num_02 float64 = 4.23
		num_03 bool    = true
	)

	var str_01 string = strconv.FormatInt(int64(num_01), 10)
	fmt.Printf("str_01对应的类型是: %T , str_01 = %q \n", str_01, str_01)
	fmt.Println("str_01 = ", unsafe.Sizeof(str_01))

	var str_02 string = strconv.FormatFloat(num_02, 'f', 9, 64)
	fmt.Printf("str_02对应的类型是: %T , str_02 = %q \n", str_02, str_02)

	var str_03 string = strconv.FormatBool(num_03)
	fmt.Printf("str_03对应的类型是: %T , str_03 = %q \n", str_03, str_03)

}
