package main

import (
	"fmt"
)

func main() {
	// 定义一个字符串
	var str string = "golang golang"
	// for循环，按照字节进行遍历输出的
	for i := 0; i < len(str); i++ {
		// fmt.Printf("%c", str[i])
		fmt.Printf("%c\n", str[i])
	}
	fmt.Println()
	fmt.Println("golang golang")

	fmt.Println()
	fmt.Println("方式二：")
	var str00 string = "I like huanhuan"
	for i, value := range str00 {
		fmt.Printf("索引为: %d, 具体的值为:%c \n", i, value)
	}
	/*
	   对str进行遍历，遍历的每个结果的索引值被i接收，每个结果的具体数值被value接收
	   遍历对字符进行遍历的
	*/
	fmt.Println()
	var huan string = "huan01"
	for i, value := range huan {
		fmt.Printf("索引为：%d,  具体的值为：%c \n", i, value)
	}
	fmt.Println()
	fmt.Println("有汉字的情况！")
	var li string = "I love 欢欢"
	for i, value := range li {
		fmt.Printf("索引为:%d, 具体的值为：%c \n", i, value)
	}
	fmt.Println()

	fmt.Println("复习")
	var shui string = "shui税"
	for i, value := range shui {
		fmt.Printf("索引：%d， 值： %c \n", i, value)
	}
	fmt.Println()
	fmt.Println("复习2")
	var white string = "认真的练习"
	for i, value := range white {
		fmt.Printf("i = %d,   value = %c \n", i, value)
	}
	for i := 0; i < len(white); i++ {
		fmt.Printf("%p", white[i])
	}
}
