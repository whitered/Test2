package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var (
		n1   int = 8
		num2 int
		num3 = "tom"
	)

	fmt.Println(n1)
	fmt.Println(num2)
	fmt.Println(num3)

	sex := "男"
	fmt.Println(sex)
	fmt.Println()

	fmt.Println("新的开始")
	var (
		// number int8 = 233
		number int8 = 24
		one01  int8 = -12
		// one02  int8 = -129
		one03 int16 = 235
		one04 int32 = 32
		one64 int64 = 64
	)
	fmt.Println(number)
	fmt.Println("one01 = ", one01)
	// fmt.Println("one02 = ", one02)
	fmt.Println("one03 = ", one03)
	fmt.Println("one04 = ", one04)
	fmt.Println("one64 = ", one64)

	// 其他整数类型 int uint rune byte
	fmt.Printf("one03的类型是: %T \n", one03)
	fmt.Println(unsafe.Sizeof(one03))

	var (
		two   int = 120
		three uint
		four  rune
		five  byte
	)
	fmt.Printf("two的类型是: %T \n", two)
	fmt.Println(unsafe.Sizeof(two))

	fmt.Printf("three的类型是: %T \n", three)
	fmt.Println(unsafe.Sizeof(three))

	fmt.Printf("four的类型是: %T \n", four)
	fmt.Println(unsafe.Sizeof(four))

	fmt.Printf("five的类型是: %T \n", five)
	fmt.Println(unsafe.Sizeof(five))
	fmt.Println()

	// 无符号整数类型 uint8 uint16 uint32 uint64
	fmt.Println("无符号的整数类型")
	var (
		six   uint8  = 8
		six00 uint16 = 16
		six32 uint32 = 32
		six64 uint64 = 64
	)

	fmt.Println("six = ", six)
	fmt.Printf("six的类型是: %T \n", six)
	fmt.Println("six占用存储空间: ", unsafe.Sizeof(six), "\n")

	fmt.Println("six00 = ", six00)
	fmt.Printf("six00的类型是: %T \n ", six00)
	fmt.Println("six00占用存储空间: ", unsafe.Sizeof(six00), "\n")

	fmt.Println("six32 = ", six32)
	fmt.Printf("six32的类型是: %T \n", six32)
	fmt.Println("six32占用存储空间: ", unsafe.Sizeof(six32), "\n")

	fmt.Println("six64 = ", six64)
	fmt.Printf("six64的类型是: %T \n ", six64)
	fmt.Println("six64占用存储空间: ", unsafe.Sizeof(six64))

}
