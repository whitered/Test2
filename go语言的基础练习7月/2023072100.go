package main

import (
	"fmt"
)

func main() {
	// 定义一个字符串
	var str string = "hello golang"
	// 方式一：普通for循环：按照字节进行遍历输出
	for i := 0; i < len(str); i++ {
		fmt.Printf("%c", str[i])
	}
}
