package main

import (
	"fmt"
	"strconv"
)

func main() {
	// string --> bool
	var (
		str01 string = "true"
		b     bool
        str_01 string = "false"
        bool_01 bool

	)

	b, _ = strconv.ParseBool(str01)
	fmt.Printf("b的类型是: %T , b = %v \n", b, b)

    // str_01 -> bool_01
    bool_01, _ = strconv.ParseBool(str_01)
    fmt.Printf("bool_01的类型是: %T , bool_01 = %v \n", bool_01, bool_01)

}
