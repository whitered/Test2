package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 类型转换
	var (
		n1  float32 = 100
		n01 float32 = n1

		// int64 int8
		two64  int64 = 888888
		three8 int8  = int8(two64)
	)

	fmt.Println(n01)
	fmt.Printf("n1的类型是: %T \n", n1)
	fmt.Printf("n01的类型是: %T", n01)
	fmt.Println()

	// 输出的值溢出
	fmt.Println("two64 = ", two64)
	fmt.Printf("two64的类型是: %T \n", two64)
	fmt.Println("two64占用存储空间: ", unsafe.Sizeof(two64))
	fmt.Println()

	fmt.Println("three8 = ", three8)
	fmt.Printf("three8的类型是: %T \n", three8)
	fmt.Println("three8占用存储空间: ", unsafe.Sizeof(three8))

}
