package main

import "fmt"

func main() {
	// 关系运算符：==、!= 、  > 、  < 、 >= 、 <=
	// 关系运算符的结果都是bool类型， true false
	// 关系表达式经常用在流程控制中
	fmt.Println(5 == 9)
	fmt.Println(3 != 5)
	fmt.Println(3 > 6)
	fmt.Println(3 < 8)
	fmt.Println(2 >= 1)
	fmt.Println(3 <= 6)

	var i int = 4
	if i < 6 {
		fmt.Println("你还需要努力啊！")
	} else {
		fmt.Println("完成目标")
	}

    
}
