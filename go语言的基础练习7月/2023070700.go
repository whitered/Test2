package main

import (
	"fmt"
)

func main() {
	var n1 int = 19
	fmt.Printf("%d", n1)
	fmt.Printf("n1对应的类型是: %T, n1 = %d \n", n1, n1)
	var s1 string = fmt.Sprintf("%d", n1)
	fmt.Printf("s1对应的类型是: %T, s1 = %q", s1, s1)
	fmt.Println()

	var n2 float32 = 3.45
	var s2 string = fmt.Sprintf("%f", n2)
	fmt.Printf("s2对应的类型是: %T, s2 = %q", s2, s2)
	fmt.Println()

	var n3 bool = false
	var s3 string = fmt.Sprintf("%t", n3)
	fmt.Printf("s3对应的类型是: %T, s3 = %q", s3, s3)
	fmt.Println()

	var n4 byte = 'a'
	var s4 string = fmt.Sprintf("%c", n4)
	fmt.Printf("s4对应的类型是: %T, s4 = %q", s4, s4)
	fmt.Println()

	fmt.Println("新的案例：2")

}
