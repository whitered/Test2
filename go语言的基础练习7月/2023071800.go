package main

import (
	"fmt"
)

func main() {
	var age int = 35
	fmt.Println("age对应的存储空间的地址为: ", &age)

	var ptr *int = &age
	fmt.Println("ptr = ", ptr)
	fmt.Println("ptr这个指针指向的具体数值为: ", *ptr)

	// 实现键盘输入信息的功能
	var age01 int
	fmt.Println("请输入年龄: ")
	fmt.Scanln(&age01)
	// fmt.Printf("age01 = %v", age01)

	var name string
	fmt.Println("名字：")
	fmt.Scanln(&name)

	var score float32
	fmt.Println("成绩: ")
	fmt.Scanln(&score)

	var isVIP bool
	fmt.Println("会员：")
	fmt.Scanln(&isVIP)
	// 打印输出
	fmt.Printf("学生的年龄为：%v, 姓名为: %v, 成绩为: %v , 是否是VIP, %v", age01, name, score, isVIP)

}
