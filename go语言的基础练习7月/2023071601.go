package main

import (
	"fmt"
)

func main() {
	// 多级判断；判断学生的等级
	var score int = 32
	/* 利用if单分支
	if score >= 90 {
		fmt.Println("您的成绩为A级别")
	}
	if score >= 80 && score < 90 {
		fmt.Println("您的成绩为B级别")
	}
	if score >= 70 && score < 80 {
		fmt.Println("您的成绩为C级别")
	}
	if score >= 60 && score < 70 {
		fmt.Println("您的成绩为D级别")
	}
	if score < 60 {
		fmt.Println("您的成绩为E级别")
	}
	*/

	// 多分支，如果已经走了一个分支，那么下面的分支就不会咋去判断
	if score >= 90 {
		fmt.Println("您的成绩为A级别")
	} else if score >= 80 {
		fmt.Println("您的成绩为B级别")
	} else if score >= 70 {
		fmt.Println("您的成绩为C级别")
	} else if score >= 60 {
		fmt.Println("您的成绩为D级别")
	} else if score <= 60 {
		fmt.Println("您的成绩为E级别")
	}

	var book int = 10
	if book > 40 {
		fmt.Println("我拥有的书是: ", book)
	} else if book < 40 {
		fmt.Println("拥有的书少于40本")
	}

	// switch
	switch score / 10 {
	case 10:
		fmt.Println("您的成绩为A级")
	case 9:
		fmt.Println("您的成绩为A级")
	case 8:
		fmt.Println("您的成绩为B级")
	case 7:
		fmt.Println("您的成绩为C级")
	case 6:
		fmt.Println("您的成绩为D级")
	case 5, 4, 3, 2, 1, 0:
		fmt.Println("您的成绩为E级")
	default:
		// 成绩有误
		fmt.Println("您的成绩有误！")
	}

}
