package main

import (
	"fmt"
)

func main() {
	test()
	fmt.Println("精通Golang")
	fmt.Println("拿到1.6W")
}

func test() {
	// 利用defer+recover来捕获错误：defer后加上匿名函数调用
	defer func() {
		// recover来捕获错误
		errors := recover()
		// 如果没有捕获错误，返回值为零值：nil
		if errors != nil {
			fmt.Println("错误已经捕获")
			fmt.Println("errors = ", errors)
		}
	}()
	num1 := 10
	// num2 := 2
	num2 := 0
	result := num1 / num2
	fmt.Println(result)
}
