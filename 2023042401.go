package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 测试布尔类型的数值
	var flag01 bool = true
	fmt.Println(flag01)

	var flag02 bool = false
	fmt.Println(flag02)

	var flag03 bool = 5 < 9
	fmt.Println(flag03)
	fmt.Println()
	var newBool bool = 5 > 12
	fmt.Println(newBool)
	fmt.Println(unsafe.Sizeof(newBool))
}
