package main

import (
	"fmt"
)

// &取内存地址
// *根据地址取值

func main() {
	var age int = 18
	// &符号 + 变量 就可以获取这个变量的地址
	fmt.Println(&age) // 0xc00010a000
	// fmt.Println("age的值是: ", age)
	// fmt.Printf("age的数值为: %v \n", age)
	// fmt.Printf("age的数据类型为: %T \n", age)
	// fmt.Println(unsafe.Sizeof(age))

	// 定义一个指针变量
	var ptr *int = &age
	fmt.Println(ptr)
	fmt.Println(age)
	// fmt.Println(&ptr)

	fmt.Println("ptr本身这个存储空间的地址为: ", &ptr)
	// fmt.Println("ptr本身这个存储空间的地址为: ", *ptr)

	// 想获取ptr这个指针或者这个地址指向的那个数据：
	fmt.Printf("ptr指向的数值为: %v", *ptr)

	// 练习01
	fmt.Println()
	fmt.Println(" \n 理解练习01")
	var test01 float64 = 3.14
	fmt.Println(&test01)
	var book *float64 = &test01
	fmt.Println(book)
	fmt.Println("book本身这个存储空间的地址为: ", &test01)

	// 想要获取指针或者这个地址指向的那个数据
	fmt.Printf("book指向的数值为: %v", *book)
	// fmt.Println(*book)
	fmt.Println()
	fmt.Println(test01)
	// fmt.Println(*test01)

	// 练习02
	fmt.Println()
	fmt.Println(" \n 理解练习02, bool")
	var egg bool = true
	fmt.Println(&egg)
	// fmt.Println(*egg)

	var egg01 *bool = &egg
	fmt.Println(egg01)
	fmt.Println(*egg01)

	fmt.Println("egg的存储空间地址为: ", egg01)
	fmt.Println("egg的存储空间地址为: ", &egg01)
	fmt.Println("egg的数值为: ", *egg01)

	// 新的练习，指针与值的细节问题处理
	fmt.Println()
	var num int = 100
	fmt.Println(num)

	var pp *int = &num
    // 通过指针改变值
	*pp = 110
	fmt.Println(num)
	// 内存地址
	fmt.Println("pp的内存地址: ", pp)

    // 指针变量要与数据类型匹配

}
