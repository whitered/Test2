package main

import (
	"fmt"
)

func main() {
	var sum int = 0
	for i := 1; i <= 100; i++ {
		sum += i
		fmt.Println(sum)
		if sum >= 300 {
			// 停止正在执行的这个循环
			break
		}
	}
	fmt.Println("-----ok-----")
}
