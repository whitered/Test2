package main

import (
	"fmt"
)

func main() {
	// 功能：根据给出的学生分数，判断学生的等级
	// 方式一：利用if但分支实现
	// 定义一个学生的成绩
	var score int = 88
	// 对学生的成绩进行
	// if score >= 90 {
	// 	fmt.Println("您的成绩为A级别")
	// }
	// if score >= 80 {
	// 	fmt.Println("您的成绩为B级别")
	// }
	// if score >= 70 {
	// 	fmt.Println("您的成绩为C级别")
	// }
	// if score >= 60 {
	// 	fmt.Println("您的成绩为D级别")
	// }
	// if score < 60 {
	// 	fmt.Println("您的成绩为E级别")
	// }
	/** 备注：利用多个单分支拼凑出多个选择，多个选择是并列的，依次从上而下顺序执行，
	 即使走了第一个分支，那么其它分支也是需要判断**/

	// 方式二：多分支
	// 优点：如果已经走了一个分子，那么下面的分支就不会再去判断执行了


}
