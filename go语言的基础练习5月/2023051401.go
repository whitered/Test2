package main

import (
	"fmt"
)

func main() {
	var age int
	// fmt.Println("请输入学生的年龄: ")
	// fmt.Scanln(&age)

	var name string
	// fmt.Println("请输入学生的名字: ")
	// fmt.Scanln(&name)

	var score float32
	// fmt.Println("请输入学生的成绩: ")
	// fmt.Scanln(&score)

	var isVIP bool
	// fmt.Println("请输入学生是否为VIP: ")
	// fmt.Scanln(&isVIP)

	// 将上述数据在控制台打印输出
	// fmt.Printf("学生的年龄为: %v, 姓名为: %v, 成绩为: %v, 是否为VIP: %v", age, name, score, isVIP)

	fmt.Println("请输入学生的年龄，姓名，成绩，是否是VIP，使用空格进行分隔")
	fmt.Scanf("%d %s %f %t", &age, &name, &score, &isVIP)
	// 打印输出
	fmt.Printf("学生的年龄为: %v, 姓名为: %v, 成绩为: &v, 是否为VIP: %v", age, name, score, isVIP)

}
