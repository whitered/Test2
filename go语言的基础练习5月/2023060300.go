package main

import (
	"fmt"
)

func main() {
	// 功能：输出1-100中能被5整除的数
	var count int = 0
	for i := 1; i <= 100; i++ {
		if i%5 == 0 {
			fmt.Println(i)
			count++
		}

	}
    // 能被5整除的数的个数，  循环的次数
	fmt.Println("count = ", count)

}
