package main

import (
	"fmt"
)

func main() {
	// 功能：双重循环 i = 1 i <= 5 j=2 j<=4，当i=2,j=2 跳出循环
	for i := 1; i <= 5; i++ {
		// fmt.Println("外层循环++++")
		for j := 2; j <= 4; j++ {
			// fmt.Println("内层循环++")
			if i == 2 && j == 2 {
				continue
			}
			fmt.Printf("i: %v, j: %v \n", i, j)

		}
	}
	fmt.Println("-------ok--------")
}
