package main

import (
	"fmt"
)

func main() {
	var huanhuan int = 20
	if huanhuan < 30 {
		fmt.Println("huanhaun < 30")
	}

	var chengJi int = 100
	if chengJi < 60 {
		fmt.Println("不合格")
	}
	fmt.Println("合格")

	fmt.Println("-----------new-------------")

	var time int = 144
	if time > 10 {
		fmt.Println("完成任务")
	}

	// if count := 20; count < 30 {
	// 	fmt.Println("库存不足")
	// }

	if date := 31; date < 30 {
		fmt.Println("date不足")
	}
}
