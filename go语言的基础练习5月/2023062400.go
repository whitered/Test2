package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, polarisxu, from studygolang.com")
	var age int
	age = 34
	fmt.Println("age=", age)

	var number int = 6
	fmt.Println("倒计时 = ", number)

	// 超出变量的范围
	// var chao_chu int = 300.3
	// fmt.Println("超出", chao_chu)

    
}
