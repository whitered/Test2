package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello golang1")
	fmt.Println("Hello golang2")
	if 1 == 1 {
		goto label11
	}
	fmt.Println("Hello golang3")
	fmt.Println("Hello golang4")
	fmt.Println("Hello golang5")
	fmt.Println("Hello golang6")
	fmt.Println("Hello golang7")
    
label11:
	fmt.Println("Hello golang9")
	fmt.Println("Hello golang10")
	fmt.Println("Hello golang11")
	fmt.Println()

	// 数字
	fmt.Println("1")
	fmt.Println("2")
	fmt.Println("3")
	if 2 == 2 {
		goto label2
	}
	fmt.Println("4")
	fmt.Println("5")
label2:
	fmt.Println("6")
	fmt.Println("7")
	fmt.Println()

	// 案例2
	fmt.Println("one")
	fmt.Println("two")
	if 3 == 3 {
		goto label13
	}
	fmt.Println("three")
	fmt.Println("four")
label13:
	fmt.Println("five")
	fmt.Println("six")
	fmt.Println("seven")
	fmt.Println()

	// 案例三
	fmt.Println("red")
	if 6 == 6 {
		goto labell34
	}
	fmt.Println("blue")
	fmt.Println("yellow")
	fmt.Println("white")
	fmt.Println("orange")
labell34:
	fmt.Println("black")
	fmt.Println("golden")

}
