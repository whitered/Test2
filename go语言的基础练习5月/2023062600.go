package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var one int8 = 127
	fmt.Println(one)

	// var two int8 = -129
	// fmt.Println(two)

	// var three uint8 = -250
	// fmt.Println(three)

	var four = 40
	fmt.Printf("four的类型是: %T \n", four)
	fmt.Println("four的值是: ", four)
	fmt.Println()
	fmt.Println(unsafe.Sizeof(four))

	// 浮点数
	var float_one float32 = 3.14
	fmt.Println(float_one)
	fmt.Printf("float_one的类型是: %T \n", float_one)
	fmt.Println(unsafe.Sizeof(float_one))

	// 字符型
	var char01 byte = 'A'
	fmt.Println("char01的值是: ", char01)
	fmt.Printf("char01的类型是: %T", char01)
	fmt.Println()
	fmt.Println("占用空间: ", unsafe.Sizeof(char01))

	// 汉字占用空间的细节
	var char02 uint = '欢'
	fmt.Println("char02的值是: ", char02)
	fmt.Printf("char02的类型是: %T \n", char02)
	fmt.Println("占用空间: ", unsafe.Sizeof(char02))

	var c5 byte = 'A'
	fmt.Printf("c5对应的具体字符为: %c", c5)

	// 测试布尔类型
	var flag01 bool = true
	fmt.Println()
	fmt.Println(flag01)

	var flag011 bool = 3 > 5
	fmt.Println(flag011)

}
