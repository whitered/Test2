package main

import (
	"fmt"
)

// 求和-函数
func cal(num1 int, num2 int) int {
	var sum int = 0
	sum += num1
	sum += num2
	return sum
}

func main() {

	var num3 int = 30
	var num4 int = 4
	// 求和
	/*var sum1 int = 0
	sum1 += num3
	sum1 += num4
	fmt.Println(sum1)
	*/

	sum := cal(num3, num4)
	fmt.Println(sum)
}
