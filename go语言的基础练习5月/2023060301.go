package main

import (
	"fmt"
)

func main() {

	// 定义一个数来计数
	var number int = 0
	// 功能：输出能被5整除的数
	for i := 1; i <= 100; i++ {
		if i%5 != 0 {
			// 结束本次循环，继续下次循环
			continue
		}
		fmt.Println(i)
        // 每循环一次，+1
		number = number + 1
	}
    // 输出循环次数
	fmt.Println("number = ", number)
}
