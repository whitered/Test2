package main

import (
	"fmt"
)

func main() {
	// var i1 int = 1
	// var i2 int = 2
	// var i3 int = 3
	// var i4 int = 4
	// var i5 int = 5

	// var sum int = 0

	// sum = i1 + i2 + i3 + i4 + i5
	// 方案 二
	// sum += i1
	// sum += i2
	// sum += i3
	// sum += i4
	// sum += i5

	// 方案三
	// var i int = 1

	// 求和  , 定义一个变量，用来接收这个和
	// var sum int = 0
	// sum += i
	// i++
	// sum += i
	// i++
	// sum += i
	// i++
	// sum += i
	// i++
	// sum += i
	// i++

	// // 输出的结果
	// fmt.Println(sum)

	// 三次的结果一样
	// 最佳解决方案

	// var sum int = 0
	// for i := 1; i <= 5; i++ {
	// 	sum += i
	// }
	// fmt.Println(sum)

	var he int = 0
	for i := 1; i <= 50; i++ {
		he += i
	}

	fmt.Println("1加到50的和是: ", he)

}
