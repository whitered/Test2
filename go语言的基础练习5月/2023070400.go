package main

import (
	"fmt"
)

func main() {
	// 进行类型转换
	var num_1 int = 100
	fmt.Println("num_1的值是: ", num_1)
	// var num_01 float32 = num_1
	// fmt.Println("num_01的值是: ", num_01)

	var num_2 float32 = float32(num_1)
	fmt.Println("num_2的值是: ", num_2)

	fmt.Printf("num_1的类型是: %T \n", num_1)
	fmt.Printf("num_2的类型是: %T", num_2)
	fmt.Println()

	fmt.Println("案例二")
	// 转换的时间溢出问题
	var num_3 int64 = 777777
	var num_4 int8 = int8(num_3)
	fmt.Println("num_4的值是:", num_4)
	fmt.Println()

	// 左右数据类型要相等
	var num_5 int32 = 12
	var num_6 int64 = int64(num_5) + 30
	fmt.Println("num_5的值是: ", num_5)
	fmt.Println("num_6 = num_5 + 30的值是:", num_6)

	// 错误的案例
	var num_7 int64 = 12
	var num_8 int8 = int8(num_7) + 30
	fmt.Println("num_8的值是: ", num_8)

}
