package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 进行类型转换；
	var n1 int = 100
	fmt.Printf("n1对应的默认的类型为：%T \n", n1)
	fmt.Println(unsafe.Sizeof(n1))

	fmt.Println()
	// 在这里自动转换，
	// var n2 float32 = n1
	// fmt.Println(n2)

	var n2 = float32(n1)
	fmt.Println("n2的值是： ", n2)
	fmt.Printf("n2的数据类型是： %T \n", n2)
	fmt.Println(unsafe.Sizeof(n2))
	fmt.Println()

	// n1的类型其实还是int类型，只是将n1的值：100转为了float32而已，n1还是int的类型
	fmt.Printf("n1的类型为：%T \n", n1)
	fmt.Printf("n2的类型为：%T", n2)
	fmt.Println()

	// 将int64转为int8的时候，编译不会出错，但是会数据溢出
	var n3 int64 = 888888
	fmt.Printf("n3的数据类型是： %T \n", n3)
	var n4 int8 = int8(n3)
	fmt.Printf("n4的数据类型是： %T \n", n4)
	fmt.Println("n4的值是： ", n4)

	// 一定要匹配 = 左右的数据类型
	var n5 int32 = 12
	var n6 int64 = int64(n5) + 30
	fmt.Println("n5 = ", n5)
	fmt.Println("n6 = ", n6)
	fmt.Printf("n5的数据类型是： %T \n", n5)
	fmt.Printf("n6的数据类型是： %T \n", n6)
	fmt.Println(unsafe.Sizeof(n6))

	var a int
	fmt.Println("a的默认值是： ", a)
	fmt.Println(unsafe.Sizeof(a))
	var b float32
	fmt.Println("b的默认值是： ", b)
	fmt.Println(unsafe.Sizeof(b))
	var c float64
	fmt.Println("c的默认值是： ", c)
	var d bool
	fmt.Println("d的默认值是： ", d)
	var e string
	fmt.Println("e是String类型，它的默认值是：", e)

}
