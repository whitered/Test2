package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num_1 int = 19
	var num_2 float32 = 4.78
	var num_3 bool = false
	var num_4 byte = 'a'

	var str_1 string = fmt.Sprintf("%d", num_1)
	fmt.Printf("str_1对应的类型是: %T, str_1 = %q \n", str_1, str_1)

	var str_2 string = fmt.Sprintf("%f", num_2)
	fmt.Printf("str_2对应的类型是: %T, str_2 = %q \n", str_2, str_2)

	var str_3 string = fmt.Sprintf("%t", num_3)
	fmt.Printf("str_3对应的类型是: %T, str_3 = %q \n", str_3, str_3)

	var str_4 string = fmt.Sprintf("%c", num_4)
	fmt.Printf("str_4对应的类型是: %T, str_4 = %q \n", str_4, str_4)

	var num_01 int = 18
	var str_01 string = strconv.FormatInt(int64(num_01), 10)
	fmt.Printf("str_01对应的类型是: %T, str_01 = %q \n ", str_01, str_01)

	var n2 float64 = 4.29
	var s2 string = strconv.FormatFloat(n2, 'f', 9, 64)
	fmt.Printf("s2对应的类型是: %T, s2 = %q \n", s2, s2)

	var n3 bool = true
	var s3 string = strconv.FormatBool(n3)
	fmt.Printf("s3对应的类型是: %T , s3 = %q \n", s3, s3)

}
