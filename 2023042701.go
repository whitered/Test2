package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 1、定义一个字符串
	var s1 string = "你好！全面拥抱Go!!"
	fmt.Println(s1)

	var huanhuan string = "欢欢"
	fmt.Println(huanhuan)

	var lisi string = "李四"
	fmt.Println(lisi)

	// 2、字符串是不可变的：指的是字符串一旦定义好，其中的字符的值不能改变
	var s2 string = "abc"
	s2 = "def"
	// s2[0] = 't'
	fmt.Println(s2)

	fmt.Println("-----------------")
	fmt.Println(unsafe.Sizeof(s2))
	fmt.Println(unsafe.Sizeof(lisi))
	fmt.Println(unsafe.Sizeof(huanhuan))
	fmt.Println("-----------------")
	// 3、字符串的表示形式
	// 如果字符串没有特殊字符，字符串的表示形式用双引号;
	var s3 string = "sdjkdjfksdjfk"
	fmt.Println(s3)
	// 如果字符串有特殊字符，字符串的表示形式反引号
	var s4 string = `
        package main

        import (
            "fmt"
        )

        func main() {
            // 1、定义一个字符串
            var s1 string = "你好！全面拥抱Go!!"
            fmt.Println(s1)
            // 2、字符串是不可变的：指的是字符串一旦定义好，其中的字符的值不能改变
            var s2 string = "abc"
            s2 = "def"
            // s2[0] = 't'
            fmt.Println(s2)

            // 3、字符串的表示形式
            // 如果字符串没有特殊字符，字符串的表示形式用双引号;
            var s3 string = "sdjkdjfksdjfk"
            fmt.Println(s3)
            // 如果字符串有特殊字符，字符串的表示形式反引号
        }
    `
	fmt.Println(s4)

	// 4、字符串的拼接效果：
	var s5 string = "Are " + "you ok!"
	s5 += "Yes,This is good!"
	fmt.Println(s5)

	// 当一个字符串过长的时候：注意：+ 保留在上一行的最后
	var s6 string = "abc" + "def" + "abc" + "def" + "abc" + "def" +
		"abc" + "def" + "abc" + "def" + "abc" + "def" +
		"abc" + "def" + "abc" + "def" + "abc" + "def" +
		"abc" + "def" + "abc" + "def" + "abc" + "def"
	fmt.Println(s6)

	// 字符串的拼接
	var lihuan string = "zhangSan " + "liSi"
	fmt.Println(lihuan)

	// 字符串的拼接 -- 3
	var newBook string = "shuiQiang " + "love"
	newBook += "liHuan"
	fmt.Println(newBook)
}
