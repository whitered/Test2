package main

import (
	"fmt"
)

func main() {
	//单分支
	var age int = 36
	if age > 18 {
		fmt.Println("你已经成年了，你的年龄是：", age, "岁。")
	}

	var week string = "Sunday"
	if week == "Sunday" || week == "Saturday" {
		fmt.Println("假期就应该陪陪家人")
	}

	//多分支
	fmt.Println("if - else -")
	var ageTwo int = 37
	if ageTwo > 18 {
		fmt.Println("你的年龄满足参军要求！")
	} else {
		fmt.Println("你的年龄还不满足参军的年龄要求！")
	}

	var color string = "green"
	if color == "green" {
		fmt.Println("这是我最喜欢的颜色", color, "它代表没有故障，测试顺利通过！")
	} else {
		fmt.Println("现在的颜色是：", color)
	}

	var weekTwo string = "Sunday"
	if weekTwo == "Sunday" || weekTwo == "Saturday" {
		fmt.Println("周日如果放假，应该多陪陪家人，家人很重要！")
	} else {
		fmt.Println("工作日应该认真做事情，让自己能体现自己的价值！")
	}

	fmt.Println("----------多分支 if - else if - else--------------")
	score := 89
	if score >= 90 {
		fmt.Println("成绩等级为A")
	} else if score >= 80 {
		fmt.Println("成绩等级为B")
	} else if score >= 70 {
		fmt.Println("成绩等级为C")
	} else if score >= 60 {
		fmt.Println("成绩等级为D")
	} else {
		fmt.Println("成绩等级为E 成绩不及格")
	}

	fmt.Println("选择颜色代表的意思")
	colorTwo := "red"
	if colorTwo == "green" {
		fmt.Println(colorTwo, "生命和希望、自然和环保、和平和安全")
	} else if colorTwo == "red" {
		fmt.Println(colorTwo, "象征幸福、快乐和热烈")
	} else if colorTwo == "blue" {
		fmt.Println(colorTwo, "宁静与和平、智慧与理性、信任与可靠")
	} else if colorTwo == "yellow" {
		fmt.Println(colorTwo, "辉煌、光明、富贵、温暖、尊贵")
	} else {
		fmt.Println(colorTwo, "更多美好的意义")
	}

}
