package main

import "fmt"

func main() {
	//switch 语句的案例操作
	//根据分数判断等级 90 <= x < 100 A / 80 <= x < 90 B / 70 <= x < 80 C  / 60 <= x < 70 D / x < 60 E
	//var score int = 30
	score := 20
	switch score / 10 {
	case 10, 9:
		fmt.Println("等级为:A")
	case 8:
		fmt.Println("等级为:B")
	case 7:
		fmt.Println("等级为:C")
	case 6:
		fmt.Println("等级为:D")
	case 5, 4, 3, 2, 1:
		fmt.Println("等级为:E")
	}

	switch Expr(2) {
	case Expr(1), Expr(2), Expr(3):
		fmt.Println("case1")
		//fallthrough
	case Expr(4):
		fmt.Println("case2")
	}

	fmt.Println("for循环的案例")
	//显示功能：求和
	//定义变量 5个变量
	var i1 int = 1
	var i2 int = 2
	var i3 int = 3
	var i4 int = 4
	var i5 int = 5

	//求和，定义一个变量接收数据
	var sum int = 0
	sum += i1
	sum += i2
	sum += i3
	sum += i4
	sum += i5
	//输出的结果， 备注：定义的变量太多，相加的次数多
	fmt.Println("相加的和为：", sum)

	fmt.Println("方法二")
	//定义一个变量
	var j int = 1

	//求和，定义一个变量，接收这个和
	var sum1 int = 0
	sum1 += j
	j++
	sum1 += j
	j++
	sum1 += j
	j++
	sum1 += j
	j++
	sum1 += j
	j++
	//输出结果,重复的代码太多
	fmt.Println("方法二的和为：", sum1)

}

func Expr(i int) int {
	fmt.Println(i)
	return i
}
