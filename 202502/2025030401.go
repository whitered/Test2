package main

import "fmt"

func main() {
	fmt.Println("----------流程控制------------")
	ifDemo1(91)
	ifDemo2()
	fmt.Println("-------for-------")
	forDemo()
	fmt.Println()
	forDemo2()
	fmt.Println("-------forDemo3-------")
	forDemo3()

}

func ifDemo1(score int) {
	//score := 65
	if score >= 90 {
		fmt.Println("A")
	} else if score > 75 {
		fmt.Println("B")
	} else {
		fmt.Println("C")
	}
}

func ifDemo2() {
	if score := 65; score >= 90 {
		fmt.Println("A")
	} else if score > 75 {
		fmt.Println("B")
	} else {
		fmt.Println("C")
	}
}

func forDemo() {
	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}
}

// for循环的初始语句可以被忽略，但是初始语句后的分号必须要写
func forDemo2() {
	i := 0
	for ; i < 10; i++ {
		fmt.Println(i)
	}
}

// for循环的初始语句和结束语句都可以省略
func forDemo3() {
	i := 0
	for i < 10 {
		fmt.Println(i)
		i++
	}
}
