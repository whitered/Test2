package main

import (
	"fmt"
)

func main() {
	fmt.Println("Are you ok!")
	//进行类型转换
	var n1 int = 100
	fmt.Println(n1)
	var n2 float32 = float32(n1)
	fmt.Println(n2)
	fmt.Printf("n1 的类型是： %T\n", n1)
	fmt.Printf("n2 的类型是： %T\n", n2)

	//将int64 转为 int8 的时候，编译不会出错的，但是会数据溢出
	var n3 int64 = 88888888
	var n4 int8 = int8(n3)
	fmt.Println(n4)

	// 要匹配 = 左右两边的数据类型相同
	var n5 int32 = 12
	var n6 int64 = int64(n5) + 30
	fmt.Println(n6)

	var n7 int64 = 12
	var n8 int8 = int8(n7) + 127
	fmt.Println(n8)
}
