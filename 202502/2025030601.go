package main

import (
	//"dbutils"
	"fmt"
)

func main() {
	//dbutils.GetConn()
	fmt.Println("----------breakDemo01------------")
	breakDemo01()
	fmt.Println("----------continue02------------")
	continueDemo02()

	fmt.Println("----------数组------------")
	var a [3]int
	var b [4]int
	//a 和 b 是不同类型
	//fmt.Println(a == b)
	fmt.Println(a)
	fmt.Println(b)

	fmt.Println("----------数组初始化------------")
	var testArray [3]int
	var numArray = [3]int{1, 2}
	var cityArray = [3]string{"北京", "上海", "深圳"}
	fmt.Println(testArray, numArray, cityArray)
	fmt.Println(testArray)
	fmt.Println(numArray)
	fmt.Printf("type of numArray:%T\n", numArray)
	fmt.Println(cityArray)
	fmt.Printf("type of cityArray:%T\n", cityArray)

	fmt.Println("----------可变数组------------")
	aa := [...]int{1: 1, 2: 2, 3: 3, 4: 4}
	fmt.Println(aa)
	fmt.Printf("type of a:%T\n", aa)

	fmt.Println("----------数组的遍历------------")
	var arrayA = []string{"北京", "上海", "深圳"}
	for i := 0; i < len(arrayA); i++ {
		fmt.Println(arrayA[i])
	}

	for index, value := range arrayA {
		fmt.Println(index, value)
	}

	aTwo := [][]string{
		{"北京", "上海"},
		{"广州", "深圳"},
		{"成都", "重庆"},
	}
	fmt.Println(aTwo)
	fmt.Println(aTwo[0][1])

}

// 结束本次、继续下次循环
func continueDemo02() {
	for i := 0; i < 4; i++ {
		for j := 0; j < 5; j++ {
			if i == 2 && j == 3 {
				fmt.Println("shuchu")
				continue
			}
			fmt.Printf("外循环：%v - 内循环= %v \n", i, j)
		}
	}
}

func breakDemo01() {
BREAKDEMO01:
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			if i == 2 {
				break BREAKDEMO01
			}
			fmt.Printf("外循环：%v - 内循环= %v \n", i, j)
		}
	}
	fmt.Println("...")
}
