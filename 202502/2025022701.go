package main

import "fmt"

func main() {
	fmt.Println("----------函数------------")
	sum := cal(20, 20)
	fmt.Println(sum)
	fmt.Println()

	calZero(10, 27)
	fmt.Println()

	// 多个返回值
	add, res := calTwo(20, 50)
	fmt.Println(res, add)

	fmt.Println()
	jia, _ := calTwo(22, 33)
	fmt.Println("jia = ", jia)

	fmt.Println("------------交换两个数-------------")
	var oneA int = 100
	var twoB int = 222
	fmt.Printf("交换前的两个数： oneA = %v, twoB = %v", oneA, twoB)
	fmt.Println()
	exchangNum(oneA, twoB)
	fmt.Println()
	fmt.Printf("交换后的两个数： oneA = %v, twoB = %v", oneA, twoB)

	fmt.Println()
	// 可变参数
	fmt.Println("没有参数的情况：")
	test()
	fmt.Println("一个参数的情况：")
	test(10)
	fmt.Println("多个参数的情况：")
	test(10, 20, 30, 40, 50)

	var num int = 100
	testMain(num)
	fmt.Println("main ---------- ", num)

	fmt.Println("num的变量地址：", &num)
	testAddress(&num)
	fmt.Println(num)

	fmt.Println("------------函数变量类型-------------")
	a := testFunc
	fmt.Printf("a的类型是: %T， testFunc函数的类型是： %T \n", a, testFunc)
	aa := a(10)
	teatFuncB := testFunc(10)
	fmt.Println(aa == teatFuncB)

}

func testFunc(num int) int {
	//fmt.Println(num)
	return num
}

func testAddress(i *int) {
	*i = 222
}

func testMain(num int) {
	num = 30
	fmt.Println("testMain ----", num)
}

// 可变参数
func test(args ...int) {
	for i := 0; i < len(args); i++ {
		fmt.Println(args[i])
	}
}

// 交换两个数的函数
func exchangNum(a int, b int) {
	var t int
	t = a
	a = b
	b = t
	fmt.Printf("交换函数内的值： a = %v, b = %v", a, b)
}

func calTwo(i1 int, i2 int) (int, int) {
	sum := i1 + i2
	result := i1 - i2
	return sum, result
}

// 无返回值类型
func calZero(num1 int, num2 int) {
	var sum int = 0
	sum += num1
	sum += num2
	fmt.Println("calZero函数内的sum:", sum)
}

// 一个返回值类型
func cal(num1 int, num2 int) int {
	var sum int = 0
	sum += num1
	sum += num2
	return sum
}
