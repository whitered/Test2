package main

import "fmt"

type Lesson6 struct {
	name, target string
	Author
	author Author
}

type Author struct {
	name string
	wx   string
}

type Lesson struct {
	name, target string
	spend        int
}

func main() {
	fmt.Println("----------提升字段------------")
	Lesson10 := Lesson6{
		name:   "从0到Go语言微服务架构师",
		target: "全面掌握Go语言微服务如何落地，代码级彻底一次性解决微服务和分布式系统。",
	}
	Lesson10.author = Author{
		name: "欢喜哥",
		wx:   "write_code_666",
	}
	fmt.Println("lesson10 name:", Lesson10.name)
	fmt.Println("lesson10 target:", Lesson10.target)
	fmt.Println("lesson10 author wx:", Lesson10.wx)
	fmt.Println()

	fmt.Println("----------结构体比较------------")
	lesson11 := Lesson{
		name:   "从0到Go语言微服务架构师",
		target: "全面掌握Go语言微服务如何落地，代码级彻底一次性解决微服务和分布式系统。",
	}
	lesson12 := Lesson{
		name:   "从0到Go语言微服务架构师",
		target: "全面掌握Go语言微服务如何落地，代码级彻底一次性解决微服务和分布式系统。",
	}
	fmt.Println(lesson11.name == lesson12.name && lesson12.target == lesson12.target)
	fmt.Println(lesson11 == lesson12)

}
