package main

import "fmt"

func main() {
	fmt.Println("----------函数------------")
	fmt.Println("56+1 = ", sum(56, 1))
	printBookName()
	//比大小
	fmt.Println("----------比大小------------")
	numOne := maxBig(50, 20)
	fmt.Println("最大的值是：", numOne)

	fmt.Println("----------函数调用------------")
	var a int = 100
	var b int = 200
	var ret int
	//调用函数并返回最大值
	ret = FunctionMax(a, b)
	fmt.Printf("最大值是： %d \n", ret)

	fmt.Println("----------函数返回多个值------------")
	aOne, bOne := swap("Google", "Runoob")
	fmt.Println(aOne, bOne)
	nameOne, nameTwo := swap("huanhuan", "honghong")
	fmt.Println(nameOne, nameTwo)
	codeJava, codePhp := swap("Java", "PHP")
	fmt.Println(codeJava, codePhp)

	fmt.Println("----------可变参数函数------------")
	retOne := intSum2()
	fmt.Println(retOne)
	retTwo := intSum2(10)
	fmt.Println(retTwo)
	fmt.Println(intSum2(10, 20, 30))
	retThree := intSum2(10, 20, 30, 40)
	fmt.Println(retThree)

}

func intSum2(x ...int) int {
	fmt.Println(x)
	sum := 0
	for _, v := range x {
		sum += v
	}
	return sum
}

func swap(s, s2 string) (string, string) {
	return s, s2
}

func FunctionMax(num1 int, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

func maxBig(num1 int, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

// 函数返回一个无名变量，返回值列表的括号省略
func sum(i int, i2 int) int {
	return i + i2
}

func printBookName() {
	fmt.Println("《三国演义》")
}
