package main

import (
	"fmt"
)

func main() {
	//switch
	fmt.Println("---------1 - switch 后可接函数 --------------------")
	chinese := 88
	math := 90
	english := 95

	switch getResult(chinese, math, english) {
	case true:
		fmt.Println("考试通过")
	case false:
		fmt.Println("考试未通过")
	}

	fmt.Println()
	fmt.Println("---------2 - switch 后 无表达式 --------------------")
	score := 68
	switch {
	case score >= 90 && score <= 100:
		fmt.Println("grade A")
	case score >= 80 && score < 90:
		fmt.Println("grade B")
	case score >= 70 && score < 80:
		fmt.Println("grade C")
	case score >= 60 && score < 70:
		fmt.Println("grade D")
	case score < 60:
		fmt.Println("grade E")
	}

	fmt.Println("---------3 - fallthrough --------------------")
	s := "从0到Go语言微服务架构师"
	switch {
	case s == "从0到Go语言微服务架构师":
		fmt.Println("从0到Go语言微服务架构师")
		fallthrough
	case s == "Go语言微服务架构师核心22讲":
		fmt.Println("Go语言微服务架构师核心22讲")
	case s != "Go语言极简一本痛":
		fmt.Println("Go语言极简一本痛")
	}

	fmt.Println("---------4 - 循环语句 for ---------------")
	num := 0
	for num < 4 {
		fmt.Println(num)
		num++
	}
	fmt.Println()
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
	fmt.Println()
	str := "从0到Go语言微服务架构师"
	for index, value := range str {
		//fmt.Println(index, string(rune))
		fmt.Printf("index:%d, value:%c\n", index, value)
	}

	fmt.Println()

	fmt.Println("continue")
	for num := 1; num <= 10; num++ {
		if num%2 == 0 {
			continue
		}
		fmt.Println(num)
	}

	fmt.Println()
	fmt.Println("defer")
	defer fmt.Printf("从0到Go语言微服务架构师 defer 1\t")
	defer fmt.Printf("Go语言微服务架构师核心22讲 defer 2 \t")
	defer fmt.Printf("Go语言极简一本痛 defer \t")
	fmt.Printf("main \n")
}

func getResult(args ...int) bool {
	for _, v := range args {
		if v < 60 {
			return false
		}
	}
	return true
}
