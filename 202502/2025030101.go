package main

import "fmt"

func main() {
	fmt.Println("----------函数-------------")
	fmt.Println("56 + 1 = ", sum(56, 1))
	printBookName()
	fmt.Println("58 - 2 = ", sub(58, 2))

	fmt.Println("--------------- 多个类型一致的参数 ------------------")
	fmt.Println(show("Go语言", "java", "mysql", "Linux", "english"))

	fmt.Println("--------------- 多个类型不一致的参数 ------------------")
	PrintType(57, 3.1415, "这也终将过去")

}

func PrintType(args ...interface{}) {
	for _, arg := range args {
		switch arg.(type) {
		case int:
			fmt.Println(arg, "type is int.")
		case float64:
			fmt.Println(arg, "type is float64.")
		case string:
			fmt.Println(arg, "type is string.")
		default:
			fmt.Println(arg, "is an unknown type.")
		}
	}
}

func show(args ...string) int {
	sum := 0
	for _, item := range args {
		fmt.Println(item)
		sum += 1
	}
	return sum
}

// 参数的类型一致，只在最后一个参数后添加该类型
func sub(i int, i2 int) int {
	return i - i2
}

// 无参数类型和返回值
func printBookName() {
	fmt.Println("《程序员》")
}

// 函数返回一个无名变量，返回值列表的括号省略
func sum(i int, i2 int) int {
	return i + i2
}
