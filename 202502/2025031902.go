package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("Are you ok!")
	// 类型转换
	//var n1 int = 29
	//var n2 float32 = 3.1415926
	//var n3 bool = false
	//var n4 byte = 'a'

	//一次性声明
	//var (
	//	n1 = 19
	//	n2 = 3.1415926
	//	n3 = false
	//	n4 = 'a'
	//)

	//类型推断
	n1 := 19
	n2 := 4.78
	n3 := false
	n4 := 'a'

	var s1 string = fmt.Sprintf("%d", n1)
	fmt.Printf("s1对应的类型是： %T, \t s1 = %q \n", s1, s1)

	var s2 string = fmt.Sprintf("%f", n2)
	fmt.Printf("s2对应的类型是： %T, \t s2 = %q \n", s2, s2)

	var s3 string = fmt.Sprintf("%t", n3)
	fmt.Printf("s3对应的类型是： %T, \t s3 = %q \n", s3, s3)

	var s4 string = fmt.Sprintf("%c", n4)
	fmt.Printf("s4对应的类型是： %T, \t s4 = %q \n", s4, s4)

	var nn1 int = 18
	//参数：第一个参数必须转为 int64 类型，第二个参数指定字面值的进制形式为十进制
	var ss1 string = strconv.FormatInt(int64(nn1), 10)
	fmt.Printf("ss1对应的类型是： %T, \t ss1 = %q \n", ss1, ss1)
}
