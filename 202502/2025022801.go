package main

import "fmt"

func main() {
	fmt.Println("----------函数------------")
	a := test
	fmt.Printf("a的类型是：%T\n test函数的类型是： %T\n", a, test)

	a(10)

	fmt.Println("----------把函数本身当做一种数据类型------------")
	//test2(10, 3.19, test(2))
	//test2(20, 23.20, test(1))

	fmt.Println("----------自定义数据类型------------")
	type myInt int
	var num1 myInt = 30
	fmt.Println("num1=", num1)

	var num2 int = 30
	fmt.Println("num2=", num2)
	//num2 = int(num1)

	//test03(37, 55.55, test(10))
	test03(10, 3.2, testFunc)
	fmt.Println("-------testFunc------")
	testFunc(10)

	fmt.Println("--------test2(1,2.3)------")
	test2(2, 3.14159, testFunc)
	test2(22, 34.43, testTwo)

}

func testTwo(num int) {
	fmt.Println("-------函数 testTwo------")
}

func testFunc(i int) {
	fmt.Println("-------testFunc-----")
	fmt.Println(i)
}

func test2(i int, f float64, t func(num int)) {
	fmt.Println("---------test2----------")
}

func test(num int) int {
	fmt.Println(num)
	return num
}

type myFunc func(int)

func test03(num1 int, num2 float32, testFunc myFunc) {
	fmt.Println("---------test03--------")
	fmt.Printf("num1= %v,float32 = %v, testFunc = %v", num1, num2, testFunc)
}
