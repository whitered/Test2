package main

import "fmt"

func main() {
	//测试 循环 超过一定数量 break
	//var sum int = 0
	//for i := 1; i <= 100; i++ {
	//	sum += i
	//	fmt.Println(sum)
	//	if sum >= 100 {
	//		break
	//	}
	//}
	//fmt.Println("我要加油！ 前面还有很多精彩的事情，等着我！")

	//双重循环，理解break
	//for i := 1; i <= 10; i++ {
	//	for j := 1; j <= 5; j++ {
	//		fmt.Printf("i = %d ,j = %d \t", i, j)
	//		if j == 5 {
	//			fmt.Println()
	//		}
	//		if i == 2 && j == 2 {
	//			break
	//		}
	//	}
	//	fmt.Println()
	//}

	for i := 1; i <= 5; i++ {
		for j := 1; j <= 4; j++ {
			fmt.Printf("i:%v ,j:%v \n", i, j)
			if i == 2 && j == 2 {
				break
			}
		}
	}

label2:
	for i := 1; i <= 5; i++ {
		for j := 1; j <= 4; j++ {
			fmt.Printf("i:%v ,j:%v \n", i, j)
			if i == 2 && j == 2 {
				break label2
			}
		}
	}
	fmt.Println("结束循环")
	fmt.Println("Are you sure?")
}
