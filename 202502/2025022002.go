package main

import "fmt"

func main() {
	//switch
	fmt.Println("---------1 - switch --------------------")
	grade := "B"
	switch grade {
	case "A":
		fmt.Println("You score is between 90 and 100.")
	case "B":
		fmt.Println("You score is between 80 and 90.")
	case "C":
		fmt.Println("You score is between 70 and 60.")
	case "D":
		fmt.Println("You score is between 60 and 70.")
	default:
		fmt.Println("You score is below 60.")
	}
	fmt.Println()
	var money int = 40000
	switch money {
	case 40000:

		fmt.Println("You score is ", money, "买二手车。")
	case 50000, 60000, 70000, 80000, 90000:

		fmt.Println("You score is ", money, "买新车。")
	case 100000, 110000, 120000, 130000, 140000:

		fmt.Println("You score is ", money, "买全元的车。")
	case 150000, 160000, 170000, 180000, 190000:
		fmt.Println("You score is ", money, "买郑亚利的那种车。")
	default:
		fmt.Println("钱不够，老老实实的过日子，学技术。争取早日存够钱！")
	}
	fmt.Println()

	fmt.Println("---------2 - switch - 一个 case 多个条件-------------------")
	//month := 5
	//switch month {
	switch month := 2; month {
	case 1, 3, 5, 7, 8, 10, 12:
		fmt.Println("该月份有 31 天 ", month, " months.")
	case 4, 6, 9, 11:
		fmt.Println("该月份有 30 天 ", month, " months.")
	case 2:
		fmt.Println("该月份有 28 天 , 闰年有 29 天,", month, "months.")
	default:
		fmt.Println("输入信息有误！")
	}

}
