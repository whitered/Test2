package book

import "errors"

func ShowBookInfo(bookName, authorName string) (string, error) {
	if bookName == "" {
		return "", errors.New("bookName is empty")
	}
	if authorName == "" {
		return "", errors.New("authorName is empty")
	}
	return bookName + " " + authorName, nil
}
