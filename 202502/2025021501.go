package main

import "fmt"

/*
func 函数名 （形参列表） （返回值类型列表） {
	执行语句...
	return + 返回值列表
}
*/

// 自定义函数，功能：两个数相加
func add(a int, b int) int {
	return a + b
}

func cal(num1 int, num2 int) int {
	//var result int = 0
	//result += num1
	//result += num2
	//return result
	return num1 + num2
}
func main() {
	//调用函数
	fmt.Println(add(10, 20))

	var result int = 0
	//num1, num2 := 20, 30
	//var num1 int = 11
	//var num2 int = 22

	//result = cal(num1, num2)
	//fmt.Println(result)

	result = cal(20, 30)
	fmt.Println(result)

	fmt.Println("-------------------------------")
	i1 := 1
	for i1 <= 5 {
		fmt.Print(i1)
		fmt.Println(" - 你好，欢欢")
		i1++
	}

	// 死循环
	for {
		fmt.Println("这也终将过去")
	}

}
