package main

import "fmt"

func main() {
	fmt.Println("----------函数------------")
	sum := cal(20, 20)
	fmt.Println(sum)
}

func cal(num1 int, num2 int) int {
	var sum int = 0
	sum += num1
	sum += num2
	return sum
}
