package main

import "fmt"

func main() {
	//循环灵活
	i := 1       // 变量的初始化
	for i <= 5 { // 条件表达式，判断条件
		fmt.Println("你好 Golang") // 循环体
		i++                        // 迭代
	}

	// 死循环
	//for {
	//	fmt.Println("你好 Golang")
	//}
	fmt.Println("----------------------------------------")
	var str string = "hello golang 你好"

	for i, value := range str {
		fmt.Printf("索引为：%d,  具体的值为： %c \n", i, value)
	}

	fmt.Println("----------------------------------------")
	for i := 0; i < len(str); i++ {
		fmt.Printf("%c", str[i])
	}
	fmt.Println()

	var sum int = 0
	//for i := 1; i <= 100; i++ {
	//	sum += i
	//	fmt.Println(sum)
	//	if sum >= 300 {
	//		break
	//	}
	//}
	//fmt.Println("sum =  ", sum)
	//fmt.Println("---------------ok")

	for i := 1; i <= 50; i++ {
		sum += i
		if sum >= 300 {
			continue
		}
		fmt.Println(sum)
	}

}
