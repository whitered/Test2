package main

import "fmt"

func main() {
	//双重循环
	//label2:
	//	for i := 1; i <= 5; i++ {
	//		//label2:
	//		for j := 2; j <= 4; j++ {
	//			fmt.Printf("i : %v , j: %v \n", i, j)
	//			if i == 2 && j == 2 {
	//				fmt.Println("-----------")
	//				break label2
	//			}
	//		}
	//	}
	//	fmt.Println("-----再见 red------")

	//fmt.Println("--------continue--------")
	////var num int = 0
	//for i := 0; i < 100; i++ {
	//	if i%6 != 0 {
	//		continue
	//	}
	//	fmt.Println(i)
	//}

	var score int = 36
	if score > 10 {
		fmt.Println("score > 10")
		fmt.Println(score)
	} else if score > 6 {
		fmt.Println("score > 30")
		fmt.Println(score)
	} else {
		fmt.Println("你会出现吗？")
	}
	fmt.Println("------------1-----------")
	//switch 当作 if 使用
	var a int = 10
	switch {
	case a < 10:
		fmt.Println("a < 10")
	case a > 10:
		fmt.Println("a > 10")
	case a < 20:
		fmt.Println("a < 20")
	}
	fmt.Println("------------2-----------")

	var newNum int = 88
	switch newNum / 10 {
	case 9, 10:
		fmt.Println("A")
	case 8:
		fmt.Println("B")
		fallthrough
	case 7:
		fmt.Println("C")
	case 6:
		fmt.Println("D")
	case 5, 4, 3, 2, 1:
		fmt.Println("E")
	default:
		fmt.Println("你输入的信息有误！")
	}

	fmt.Println("------------3-----------")
	var sum int = 0
	for i := 1; i <= 5; i++ {
		sum += i
		fmt.Println("欢欢我会加油的")
	}
	fmt.Println("sum:", sum)

	fmt.Println("------------4- 遍历字符串 ----------")
	var str01 string = "huanhuan"
	for i := 0; i < len(str01); i++ {
		fmt.Printf("%c", str01[i])
	}
	fmt.Println()

	//方式二
	for _, val := range str01 {
		fmt.Printf("%c", val)
	}
	fmt.Println()
	for i, val := range str01 {
		fmt.Printf("str01[%v]= %c", i, val)
		fmt.Println()
	}
}
