package main

import (
	"fmt"
	"newfive/ThisOne/book"
	"newfive/ThisOne/dbutils"
)

func main() {
	fmt.Println("Are you ok!")
	fmt.Println("《Go语言极简一本通》")
	dbutils.GetConn()

	bookName := "《Go语言极简一本通》"
	author := "欢欢"
	bookInfo, _ := book.ShowBookInfo(bookName, author)
	fmt.Println("bookInfo:", bookInfo)

}
