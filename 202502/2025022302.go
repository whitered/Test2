package main

import (
	"fmt"
)

var s string = "Go语言微服务架构核心22讲"

func main() {
	// 循环
	fmt.Println("---------1 - defer return  --------------------")
	lesson := showLesson()

	fmt.Println("main : s = ", s)
	fmt.Println("main : lesson = ", lesson)

	fmt.Println("---------2 - goto --------------------")
	fmt.Println("从0到Go语言微服务架构师!")
	goto label
	fmt.Println("Go语言微服务架构核心22讲 222")
	//var x int = 0
	//fmt.Println("x =", x)
	fmt.Println("我知道这句不会执行")
label:
	fmt.Println("《Go语言极简一本通》3333")

}

func showLesson() string {
	defer func() {
		s = "从0到Go语言微服务架构师"
	}()
	fmt.Println("showLesson : s = ", s)
	return s
}
