package main

import (
	"errors"
	"fmt"
)

func main() {
	fmt.Println("----------解序列-------------")
	var s []string
	s = append(s, []string{"Go", "java", "mysql"}...)
	fmt.Println(s)

	fmt.Println("----------9.4 函数的返回值-------------")
	bookInfo, err := showBookInfo("《三国演义》", "罗贯中")
	fmt.Println(bookInfo, err)
	fmt.Printf("bookInfo= %s, err = %v", bookInfo, err)
	fmt.Println("----------showBookInfo 2 -------------")
	bookInfo2, err := showBookInfo2("《三国演义》", "罗贯中")
	fmt.Printf("bookINfo = %s, err = %v", bookInfo2, err)

}

// showBookInfo 2
func showBookInfo2(bookName, authorName string) (info string, err error) {
	info = ""
	if bookName == "" {
		err = errors.New("图书名称为空")
		return
	}
	if authorName == "" {
		err = errors.New("图书作者为空")
		return
	}
	//不使用 := 因为已经在返回值哪里声明了
	info = bookName + ",作者： " + authorName
	//直接返回即可
	return
}

func showBookInfo(bookName, authorName string) (string, error) {
	if bookName == "" {
		return "", errors.New("bookName is null")
	}
	if authorName == "" {
		return "", errors.New("authorName is null")
	}
	return bookName + " 作者：" + authorName, nil
}
