package main

import "fmt"

type Lesson struct {
	name   string // 名称
	target string // 学习目标
	spend  int    // 学习花费时间
}

type Person struct {
	name, gender string
	age          int
}

type Lesson4 struct {
	string
	int
}

type Author struct {
	name string
	wx   string
}

type Lesson5 struct {
	name, target string
	spend        int
	author       Author
}

func main() {
	fmt.Println("----------struct------------")
	lesson1 := Lesson{
		name:   "这是名字：《Go语言》",
		target: "目标：Go语言要精通",
		spend:  5,
	}
	lesson2 := Lesson{"名字：《Java》", "目标：熟练掌握", 30}
	fmt.Println(lesson1)
	fmt.Println("lesson2", lesson2)

	fmt.Println("----------创建匿名结构体------------")
	lesson3 := struct {
		name, target string
		spend        int
	}{
		name:   "目标：Go语言写代码",
		target: "target：精通Go语言，能用它写微服务",
		spend:  37,
	}
	fmt.Println("lesson3 = ", lesson3)

	fmt.Println("----------结构体的零值 zero value------------")
	var lesson4 = Lesson{}
	fmt.Println("结构体的零值:", lesson4)
	fmt.Println("----------结构体的零值 end------------")
	fmt.Println()

	fmt.Println("----------初始化结构体------------")
	var lesson5 = Lesson{
		name:   "从0到Go语言微服务架构师",
		target: "全面掌握Go语言服务如何落地，代码级彻底一次性微服务和分布式系统。",
	}
	fmt.Println("lesson5初始化：", lesson5)
	fmt.Println()

	fmt.Println("----------访问结构体的字段------------")
	var lesson6 = Lesson{
		name:   "Go语言学习的目标。",
		target: "目标是：学好Go,让自己能独立的做好一个项目。",
		spend:  37,
	}
	fmt.Println("lesson6 name:", lesson6.name)
	fmt.Println("lesson6 target:", lesson6.target)
	fmt.Println("lesson6 spend:", lesson6.spend)
	fmt.Println()

	var lesson7 = Lesson{}
	lesson7.name = "Go微服务"
	lesson7.target = "target：微服务架构师"
	lesson7.spend = 36
	fmt.Println("lesson7 = ", lesson7)
	fmt.Println()

	fmt.Println("----------指向结构体的指针------------")
	lesson8 := &Lesson{"微服务架构师", "做项目，写代码，成为有用的人", 38}
	fmt.Println("lesson8 name:", (*lesson8).name)
	fmt.Println("lesson8 name:", lesson8.name)
	fmt.Println()

	fmt.Println("----------匿名字段------------")
	lesson9 := Lesson4{"从0到Go语言微服务架构师", 50}
	fmt.Println("lesson9 = ", lesson9)
	fmt.Println("lesson9 string:", lesson9.string)
	fmt.Println("lesson9 int:", lesson9.int)
	fmt.Println()

	fmt.Println("----------嵌套结构体------------")
	lesson10 := Lesson5{
		name:  "从0到Go语言微服务架构师",
		spend: 50,
	}
	lesson10.author = Author{
		name: "ss",
		wx:   "write_code_666",
	}

	fmt.Println("lesson10 name: ", lesson10.name)
	fmt.Println("lesson10 spend:", lesson10.spend)
	fmt.Println("lesson10 auther name:", lesson10.author.name)
	fmt.Println("lesson10 auther wx:", lesson10.author.wx)

}
