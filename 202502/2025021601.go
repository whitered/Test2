package main

import "fmt"

func main() {
	var str string = "hello golang世界"
	for i := 0; i < len(str); i++ {
		fmt.Printf("%c", str[i])
	}

	fmt.Println()
	//方式二： for range
	for i, c := range str {
		fmt.Printf("索引为：%d,具体的值为： %c \n", i, c)
	}

	var newfile string = "税强爱欢欢"
	for i := 0; i < len(newfile); i++ {
		fmt.Printf("%c", newfile[i])
	}
	fmt.Println()

	for i, val := range newfile {
		fmt.Printf("索引为：%d,具体的值为： %c \n", i, val)
	}
	fmt.Println("-----------------------------------------")
	fmt.Println("单分支 if")
	var if01 int = 18
	if if01 <= 36 {
		fmt.Println("if01 =", if01, " ;if01小于36")
	}

	var day01 int = 28
	if day01 <= 28 {
		fmt.Println("day01 = ", day01, "小于等于28天的月是二月")
	}

	var num01 int = 300
	if num01 <= 100 {
		fmt.Println("num01 = ", num01)
	}

	fmt.Println("双分支 if else")
	var if02 int = 36
	if if01 <= 36 {
		fmt.Println("if02 =", if02, " ;  if02 <= 36")
	} else {
		fmt.Println("if02 =", if02, " ;  if02 > 36")
	}

	var day02 int = 30
	if day02 <= 28 {
		fmt.Println("day02 = ", day02, " <= 28天的月是二月")
	} else {
		fmt.Println("day02 =", day02, " ;  day02 > 28 是其它月份")
	}

	var num02 int = 300
	if num02 <= 100 {
		fmt.Println("num02 = ", num02, " ; 输出： num02 <= 100 ")
	} else {
		fmt.Println("num02 =", num02, " ;  输出：num02 > 100")
	}

	fmt.Println("-----------------------------------------")
	fmt.Println("多分支 if - else if -else ")
	// 根据分数判读等级： A B C D E
	//方式1，简单的if单分支实现 展示 找出局限性
	//局限性：利用多个单分支实现多分枝的功能，每一个分支都需要判刑执行，效率比较低
	var score int = 88
	if score >= 90 {
		fmt.Println("score = ", score, "等级为：A")
	}
	if score >= 80 && score < 90 {
		fmt.Println("score = ", score, "等级为：B")
	}
	if score >= 70 && score < 80 {
		fmt.Println("score = ", score, "等级为：C")
	}
	if score >= 60 && score < 70 {
		fmt.Println("score = ", score, "等级为：D")
	}
	if score < 60 {
		fmt.Println("score = ", score, "等级为：E")
	}

	if score >= 90 {
		fmt.Println("score = ", score, "等级为：A")
	} else if score >= 80 {
		fmt.Println("score = ", score, "等级为：B")
	} else if score >= 70 {
		fmt.Println("score = ", score, "等级为：C")
	} else if score >= 60 {
		fmt.Println("score = ", score, "等级为：D")
	} else {
		fmt.Println("score = ", score, "等级为：E")
	}

	//案例1, 3种颜色做选择：red、green、blue
	var color string = "gray"
	if color == "red" {
		fmt.Println("color = ", color, "， color = red")
	} else if color == "green" {
		fmt.Println("color = ", color, "， color = green")
	} else if color == "blue" {
		fmt.Println("color = ", color, "， color = blue")
	} else {
		fmt.Println("color = ", color, "， 其它颜色")
	}

	//案例2, 编程语言：PHP、python、Java
	var code string = "mysql"
	if code == "PHP" {
		fmt.Println("我喜欢的编程语言是：", code)
	} else if code == "python" {
		fmt.Println("我喜欢的编程语言是：", code)
	} else if code == "java" {
		fmt.Println("我喜欢的编程语言是：", code)
	} else {
		fmt.Println("编程语言是：", code)
	}

	//案例3, 回家的选择，大巴(3)、开车(2)、坐车(4)
	var time01 int = 2
	if time01 == 3 {
		fmt.Printf("回家需要的时间%d小时左右，坐大巴回家", time01)
	} else if time01 == 2 {
		fmt.Printf("回家需要的时间%d小时左右，开车回家", time01)
	} else if time01 == 4 {
		fmt.Printf("回家需要的时间%d小时左右，坐车回家", time01)
	}
}
