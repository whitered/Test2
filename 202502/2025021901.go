package main

import "fmt"

func main() {
	if a := 1; false {
	} else if b := 2; false {
	} else if c := 3; false {
	} else {
		fmt.Println(a, b, c)
	}
	fmt.Println("----------1-----------")
	for i := 1; i <= 100; i++ {
		if i%5 == 0 {
			fmt.Println(i, "is ok")
		}
	}

	fmt.Println("----------2----------")
	for i := 1; i <= 100; i++ {
		if i%5 != 0 {
			continue
		}
		fmt.Println(i, "is ok")
	}
	fmt.Println("----------3- 函数 ---------")
	//var num1 int = 10
	//var num2 int = 20
	//var sum int = 0
	//sum += num1
	//sum += num2
	//fmt.Println(sum)
	//
	//var num3 int = 30
	//var num4 int = 40
	//var sum2 int = 0
	//sum2 += num3
	//sum2 += num4
	//fmt.Println("sum2 = ", sum2)
	sum3 := cal(10, 20)
	fmt.Println("sum3 = ", sum3)

	fmt.Println("----------4- 相加 -------")
	var num5 int = 10
	var num6 int = 10
	sum01 := cal(num5, num6)
	fmt.Println("sum01 = ", sum01)

	fmt.Println("----------5- 相加 -------")
	num7 := 36
	num8 := 35
	sum03 := cal(num7, num8)
	fmt.Println("sum03 = ", sum03)

	fmt.Println("----------6-没有返回值类型-------")
	cal2(10, 30)

	fmt.Println("----------7-返回值类型 2个值-------")
	one, two := calTwo(20, 20)
	fmt.Println("add = ", one)
	fmt.Println("sub = ", two)

	fmt.Println("只接收一个值")
	three, _ := calTwo(20, 12)
	fmt.Println("three = ", three)
	_, fire := calTwo(20, 12)
	fmt.Println("fire = ", fire)

}

func calTwo(num1 int, num2 int) (int, int) {
	add := num1 + num2
	sub := num1 - num2
	return add, sub
}

func cal2(i int, i2 int) {
	var sum int = 0
	sum = i - i2
	fmt.Println(sum)
}

func cal(i int, i2 int) int {
	var result int = 0
	result += i
	result += i2
	return result
}
