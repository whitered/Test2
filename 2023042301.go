package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 练习转义字符
	// \b  换行
	fmt.Println("aaa\nbbb")
	fmt.Println("111\n222\n333")
	fmt.Println("Are you OK\n This is good!\n think about\n how about...")
	fmt.Println("------------------")

	// \b 退格
	fmt.Println("aaa\bbbb")
	fmt.Println("1111\b2222\b3333")
	fmt.Println("think\bSure\bOnly\bNew\bGood")
	fmt.Println("------------------")

	// \r 光标回到本行的开头，后续输入就会替换原有的字符
	fmt.Println("aaaaaaaaaaaaa")
	fmt.Println("aaaaa\tbbbbb")
	fmt.Println("aaaaa\tbbbbb")
	fmt.Println("aaaaaaaa\tbbbbb")
	fmt.Println("12345\t12345")
	var newNameOne = "11223\t12345"
	fmt.Println("11223\t12345")
	fmt.Println("newNameOne = ", newNameOne)
	fmt.Printf("newNameOne对应的数据类型是： %T \n", newNameOne)
	// 占用空间是多少？
	fmt.Println(unsafe.Sizeof(newNameOne))
	fmt.Println("--------end 3 ----------")

	// \"
	fmt.Println("\"Golang\"")
	var newName = "Golang"
	fmt.Println(newName)
	fmt.Printf("newName对应的数据类型是： %T \n", newName)
	fmt.Println(unsafe.Sizeof(newName))
	fmt.Println("--------end 4 ----------")

	var newNameTwo = "\""
	fmt.Println(newNameTwo)
	fmt.Printf("newNameTwo对应的数据类型是： %T \n", newNameTwo)
	fmt.Println(unsafe.Sizeof(newNameTwo))
	fmt.Println("--------end 5 ----------")

	var flag01 bool = true
	fmt.Println(flag01)

}
