package main

import (
	"fmt"
)

// 全局变量的声明于赋值
var a_all = "霜white"
var b_all string = "golang"
var c_all bool

var age01 int = 36
var jiNeng string = "go, MySQL, linux"
var time01 int = 3

func main() {
	fmt.Println("hello world")
	var int01 int
	var float02 float32
	var float03 float64
	var bool04 bool
	var srtr01 string

	fmt.Println(int01)
	fmt.Println(float02)
	fmt.Println(float03)
	fmt.Println(bool04)
	fmt.Println(srtr01)

	fmt.Println("声明变量")
	var a_int int = 10
	var b = 10
	c := 10

	fmt.Println(a_int)
	fmt.Println(b)
	fmt.Println(c)

	fmt.Println("---------------------------")
	fmt.Println(a_all)
	fmt.Println(b_all)
	fmt.Println(c_all)
	fmt.Println(a_all, b_all, c_all)

	fmt.Println("---------------------------")
	fmt.Println(age01)
	fmt.Println(jiNeng)
	fmt.Println(time01)

	//多变量声明
	var name01, name2, name3 string
	name01, name2, name3 = "go", "php", "mysql"
	fmt.Println(name01, name2, name3)

	fmt.Println()
	fmt.Println("类型转换")
	var num1 int = 100
	fmt.Println(num1)
	var fl02 float32 = float32(num1)
	fmt.Println("fl02 = ", fl02)

	fmt.Printf("num1 = %T\n", num1)

	var nun64 int64 = 88889999
	fmt.Println(nun64)
	var num32 int16 = int16(nun64)
	fmt.Println(num32)

	//运算的时间，一定要匹配 = 左右的数据类型
	var one11 int = 11
	var two22 int64 = 22
	fmt.Println(one11, two22)
}
