package main

import (
	"fmt"
	"strconv"
)

func main() {
	var n1 int = 100
	fmt.Println(n1)
	var n2 float32 = float32(n1)
	fmt.Println(n2)
	fmt.Printf("%T\n", n2)
	fmt.Println()

	// 将int64 转为 int8 的时候，编译不会出错的， 但是会数据的溢出
	var n3 int64 = 888888
	var n4 int8 = int8(n3)
	fmt.Println(n4)

	var n5 int32 = 12
	var n6 int64 = int64(n5) + 30
	fmt.Println("n5 = ", n5)
	fmt.Println("n6 = ", n6)

	var n7 int64 = 12
	var n8 int8 = int8(n7) + 127
	fmt.Println("n8 = ", n8)

	var nn1 int = 20
	var nn2 float32 = 4.72
	var nn3 bool = false
	var nn4 byte = 'a'

	var ss1 string = fmt.Sprintf("%d", nn1)
	var str01 string = strconv.FormatInt(int64(nn1), 10)
	fmt.Printf("ss1对应的类型是： %T, ss1 = %q \n", ss1, ss1)
	fmt.Printf("str01对应的类型是： %T, str01 = %q \n", str01, str01)

	var ss2 string = fmt.Sprintf("%f", nn2)
	fmt.Printf("ss2对应的类型是： %T, ss2 = %q \n", ss2, ss2)

	var ss3 string = fmt.Sprintf("%t", nn3)
	fmt.Printf("ss3对应的类型是： %T, ss3 = %q \n", ss3, ss3)

	var ss4 string = fmt.Sprintf("%c", nn4)
	fmt.Printf("ss4对应的类型是： %T, ss4 = %q \n", ss4, ss4)

	var str_01 string = "true"
	var bb bool
	bb, _ = strconv.ParseBool(str_01)
	fmt.Printf("bb的类型是： %T, bb = %v \n", bb, bb)

	var str_02 string = "19"
	var number1 int64
	number1, _ = strconv.ParseInt(str_02, 10, 64)
	fmt.Printf("number1的类型是： %T, number1 = %v \n", number1, number1)

	var age int = 18
	//&符号 + 变量 就可以获取这个变量内存的地址
	fmt.Println("内存地址：", &age)

	var ptr *int = &age
	fmt.Println(ptr)
	fmt.Println("ptr本身这个存储空间的地址为： ", &ptr)
	fmt.Println()
	fmt.Printf("ptr指向的数值为： %v \n", *ptr)

	fmt.Println()
	var one int = 10
	fmt.Println(one)

}
