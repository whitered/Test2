package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 定义字符类型的数据
	var c1 byte = 'a'
	fmt.Println(c1)
	var c2 byte = '6'
	fmt.Println(c2)
	fmt.Printf("c2的数据类型： %T", c2)
	var c3 byte = '('
	fmt.Println(c3 + 20)
	fmt.Println("c3 =", unsafe.Sizeof(c3))
	fmt.Printf("c3的数据类型： %T", c3)
	fmt.Println()

	// 中文汉字
	var c4 int64 = '中'
	fmt.Println(c4)
	fmt.Printf("c4的数据类型： %T", c4)
	fmt.Println()

	// 中文试一试int32 税
	var shui int32 = '税'
	fmt.Println(shui)
	fmt.Println("shui占的字节数：", unsafe.Sizeof(shui))
	fmt.Printf("shui的类型是： %T \n", shui)
	fmt.Println()

	// 想显示对应的字符，必须采用格式化输出
	var c5 = 'A'
	fmt.Printf("C5对应的具体的字符为： %C \n", c5)
	fmt.Println("C5占的字节数：", unsafe.Sizeof(c5))
	fmt.Printf("c5的类型是： %T \n\n", c5)

	var are byte = 'b'
	fmt.Println(are)
	fmt.Println("are占字节是：", unsafe.Sizeof(are))
	fmt.Printf("are的类型是： %T \n", are)
	fmt.Println("\nThis is test01")

	var about = 112154512124544444
	fmt.Println(about)
	fmt.Println("about占字节是：", unsafe.Sizeof(about))
	fmt.Printf("about的类型是： %T", about)

}
