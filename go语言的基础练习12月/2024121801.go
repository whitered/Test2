package main

import (
	"fmt"
	"strconv"
)

func main() {
	// 显式类型转换
	var x int = 10
	var y float64 = float64(x)
	fmt.Println(y)

	// 类型别名的转换
	type Celsius float64
	type Fahrenheit float64
	c := Celsius(25)
	f := Fahrenheit(c*9/5 + 32)
	fmt.Println(f)

	//1、变量的声明
	var age int
	//2、变量的赋值
	age = 36
	//3、变量的使用
	fmt.Println("age = ", age)

	//声明和赋值合成一句
	var age01 int = 35
	fmt.Println("age01 = ", age01)

	var age02 string = strconv.Itoa(33)
	fmt.Println("age02 = ", age02)

	//变量的4种使用方式
	//1、指定变量的类型，并且赋值
	var age03 int = 27
	fmt.Println("age03 = ", age03)

	//2、指定变量的类型，不赋值，使用默认值
	var age04 int
	fmt.Print("默认值：")
	fmt.Println(age04)

	//3、自动类型推断
	var nane00 = "waitRed"
	fmt.Println(nane00)

	//4、省略var,
	sex := "男"
	fmt.Println(sex)

	fmt.Println("----------------------------------------")
	//声明多个变量
	var n00, n01, n02 int
	fmt.Println("相同类型，输出默认值：")
	fmt.Println("n00 = ", n00)
	fmt.Println("n01 = ", n01)
	fmt.Println("n02 = ", n02)
	fmt.Println("不同类型，输出不同的值：")
	var n04, name00, n05 = 10, "huanhuan", 7.8
	fmt.Println("n04 = ", n04)
	fmt.Println("name00 名字为：", name00)
	fmt.Println("n05 = ", n05)
	fmt.Println("省略var, 通过值推断除类型")
	n06, height00 := 6.9, 100.2
	fmt.Println("n06 = ", n06)
	fmt.Println("height00 = ", height00)

	fmt.Println("全局变量")
	fmt.Println("n07 = ", n07)
	fmt.Println("n08 = ", n08)

	fmt.Println("多个全局变量一次声明和使用的方案")
	fmt.Println(n09)
	fmt.Println(n010)

}

// 全局变量，定义在函数外的变量
var n07 = 7.7
var n08 = 8.8

// 一次声明多个全局变量的情况
var (
	n09  = 900
	n010 = 10.10
)
