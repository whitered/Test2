package main

import "fmt"

func main() {
	// 字符串可以通过 “+” 链接
	fmt.Println("go" + "lang")
	fmt.Println("huan " + "huan")
	fmt.Println("shui" +" @ " + "huanHuan")
	fmt.Println()

	// 整数和浮点数
	fmt.Println("1 + 1 =", 1 + 1)
	fmt.Println("7.0 / 3.0 =", 7.0 / 3.0)
	fmt.Println("5 + 5 = ", 5+5)
	fmt.Println("10.0 / 3.0 = ", 10.0 / 3.0)
	fmt.Println("10 / 3 = ", 10 / 3)
	fmt.Println("10 % 3 = ", 10 % 3)
	fmt.Println()

	// 布尔型，还有你想要的符
	fmt.Println(true && false)
	fmt.Println(true || false)
	fmt.Println(!true)

}