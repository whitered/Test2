package main

import "fmt"

//const (
//	BEIJING  = 0
//	SHANGHAI = 1
//	SHENZHEN = 2
//)

// const 来定义枚举类型
const (
	BEIJING  = 10 * iota
	SHANGHAI = 1
	SHENZHEN = 2
)

func main() {
	//方法一：声明一个变量，默认的值是：0
	var a int
	fmt.Println("a = ", a)
	fmt.Printf("a 的值是： %d\n", a)
	fmt.Printf("a 的类型是： %T\n", a)

	//方法二：声明一个变量，初始化一个值
	var b01 int = 109
	fmt.Printf("b01 = %d , b01 的类型是: %T \n", b01, b01)
	var bb01 string = "从0到Go语言微服务架构师"
	fmt.Printf("bb01 = %s , bb01的类型是: %T \n", bb01, bb01)

	//方法三：在初始化的时候，可以省去数据类型，通过值去自动匹配当前变量的数据类型
	var cc = 100
	fmt.Printf("cc = %d , cc 的类型是: %T \n", cc, cc)

	var cc01 = "Go语言架构师"
	fmt.Printf("cc01 = %s , cc01的类型是: %T \n", cc01, cc01)

	//方法四：(常用的方法)省去var关键字，使用 := ，既推导数据类型又赋值
	//注：短声明是在函数或方法内部使用，不支持全局变量声明！！！
	e := 100
	fmt.Printf("e = %d, e的类型是：%T \n", e, e)

	ee := "Go语言极简一本通"
	fmt.Printf("ee = %s, ee的类型是: %T \n", ee, ee)

	fmt.Println("\n多变量声明")
	var xx, yy int = 110, 120
	fmt.Printf("xx = %d , yy = %d \n", xx, yy)
	var kk, wx = 330, "认证对待Go语言的小练习"
	fmt.Printf("kk = %d , wx = %d \n", kk, wx)

	fmt.Println("\n常量")
	//常量
	const length int = 100
	//length = 20  常量是不允许被修改的
	fmt.Printf("length = %d \nlength的类型是： %T \n", length, length)

	const huan string = "李欢"
	fmt.Printf("huan = %s \nhuan的类型是： %T \n", huan, huan)

	fmt.Println("\n枚举类型")
	fmt.Println("BEIJING = ", BEIJING)
	fmt.Println("SHANGHAI = ", SHANGHAI)
	fmt.Println("SHENZHEN = ", SHENZHEN)

}
