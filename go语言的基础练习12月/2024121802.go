package main

import "fmt"

func main() {
	//1、指定变量的类型，并且赋值
	var bian01 int = 36
	fmt.Println("bian01 = ", bian01)

	//2、使用默认值、给变量赋值
	var bian02 float64
	fmt.Println("bian02 = ", bian02)

	//3、通过变量的值判断类型
	var bian03 = "dengHong"
	fmt.Println("bian03 = ", bian03)

	//4、省略var 直接赋值
	bian04 := "shuan"
	fmt.Println("bian04 = ", bian04)

	//声明多个变量
	var bian05, bian06 = 50, 60
	fmt.Println("bian05 = ", bian05)
	fmt.Println("bian06 = ", bian06)

	//定义一个整数类型
	var i00 int = 8
	fmt.Println("i00 = ", i00)

	//定义一个整数类型,超出 int 的范围
	//var big int8 = 233
	//fmt.Println("big = ", big)
	var big uint = 293
	fmt.Println("big = ", big)

}
