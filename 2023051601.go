package main

import (
	"fmt"
)

func main() {
	if count := 35; count < 30 {
		fmt.Println("为了过更好的生活，加油！")
	}

	var one int = 10
	if one < 30 {
		fmt.Println("相信自己！")
	} else {
		fmt.Println("一定")
	}

}
