package main

import (
	"fmt"
)

func main() {
	var int_a int
	var float_b float32
	var float_c float64
	var bool_d bool
	var str string

	fmt.Println("int的默认值: ", int_a)
	fmt.Println("浮点类型32的默认值: ", float_b)
	fmt.Println("浮点类型64的默认值: ", float_c)
	fmt.Println("布尔类型的默认值: ", bool_d)
	fmt.Println("字符类型的默认值: ", str)

}
