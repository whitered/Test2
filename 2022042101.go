package main

import (
	"fmt"
	"unsafe"
)

func main() {
	// 浮点数可能会有进度的损失，通常情况下，建议使用：float64；默认float64
	var num_three float32 = 256.000000916
	fmt.Println(num_three)

	var numFour float64 = 256.000000916
	fmt.Println(numFour)

	// 测试 32
	var numFive float32 = 53.14000111
	fmt.Println(numFive)
	// 测试 64
	var numSix float64 = 253.14000111
	fmt.Println(numSix)

	// 默认float数据类型
	var numSeven = 35.12
	fmt.Printf("numSeven %T \n", numSeven)
	fmt.Println("numSeven占用字节数：", unsafe.Sizeof(numSeven))
	fmt.Println() // 换行

	var numEight = 1.2
	fmt.Printf("numEight =  %T", numEight)

	// 测试 浮点数64
	fmt.Println() // 换行
	var numNine = 6.5
	fmt.Printf("numNine == %T", numNine)

	fmt.Println() // 换行
	// 科学计数法
	var numTen float32 = 314e-2
	fmt.Println(numTen)

	// 科学计数法 正数
	var numEleven float32 = 3140e-2
	fmt.Println(numEleven)
	fmt.Println(unsafe.Sizeof(numEleven))
}
