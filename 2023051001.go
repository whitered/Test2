package main

import (
	"fmt"
)

func main() {
	fmt.Println(5 == 9)
	fmt.Println(5 != 9)
	fmt.Println(5 > 9)
	fmt.Println(5 < 9)
	fmt.Println(5 >= 9)
	fmt.Println(5 <= 9)

	fmt.Println(true == false)
	fmt.Println(true == true)
	fmt.Println(false == false)

	fmt.Println()
	var book string = "abc"
	fmt.Println(book)
	fmt.Println(&book)
	var newBook *string = &book
	fmt.Println(newBook)
	// newbook的存储空间
	fmt.Println("newBook的存储空间的地址为: ", &newBook)

	// 获取值
	fmt.Printf("newBook指向的数值为: %v \n", *newBook)

	// 存储空间的地址
	fmt.Printf("newBook指向的数值为: %v \n", &newBook)
	fmt.Printf("book指向的数值为: %v ", &book)

}
