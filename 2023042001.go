package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var num3 = 28
	// Printf函数的作用是：格式化的，把num3 的类型填充到%T的位置上
	fmt.Printf("num3的类型是： %T \n", num3)
	// 占用字节数
	fmt.Println(unsafe.Sizeof(num3))

	// 打印一个换行
	fmt.Println()
	var newBook = 1.21
	fmt.Printf("newBook的类型是： %T \n", newBook)
	fmt.Println(newBook)
	fmt.Println(unsafe.Sizeof(newBook))

	// 打印一个换行
	fmt.Println()
	var newPen uint16 = 314
	fmt.Printf("newPen的类型是： %T \n", newPen)
	fmt.Println(newPen)
	fmt.Println(unsafe.Sizeof(newPen))

	// 打印一个换行
	fmt.Println()
	var newNum uint32 = 3232
	fmt.Printf("newNum的数据类型是： %T \n", newNum)
	fmt.Println(newNum)
	fmt.Println("newNum占的字节数：", unsafe.Sizeof(newNum))

	// 打印一个换行
	fmt.Println()
	var newBut uint64 = 222222
	fmt.Printf("newBut的数据类型是： %T \n", newBut)
	fmt.Println(newBut)
	fmt.Println()

	// 打印一个换行
	fmt.Println()
	var numAge int = 20
	fmt.Printf("numAge的数据类型是： %T \n", numAge)
	fmt.Println(numAge)

	// 打印一个换行
	fmt.Println()
	var numAbout uint = 255
	fmt.Printf("numAbout的数据类型是： %T \n", numAbout)
	fmt.Println(numAbout)

	// 打印一个换行
	fmt.Println()
	var numApple rune = 255
	fmt.Printf("numApple的数据类型是： %T \n", numApple)
	fmt.Println(numApple)
	fmt.Println("numApple占的字节数是：", unsafe.Sizeof(numApple))

}
