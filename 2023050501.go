package main

import (
	"fmt"
	"strconv"
)

func main() {
	var n1 int = 19
	var n2 float32 = 4.71
	var n3 bool = false
	var n4 byte = 'a'

	var s1 string = fmt.Sprintf("%d", n1)
	fmt.Printf("s1对应的类型是:  %T, s1 = %q \n", s1, s1)

	fmt.Println()
	// fmt.Sprintf(n2)
	fmt.Println()
	var s2 string = fmt.Sprintf("%f", n2)
	fmt.Printf("s2对应的类型是: %T, s2 = %q \n", s2, s2)

	fmt.Println()
	fmt.Sprintf("%t", n3)
	fmt.Println()
	var s3 string = fmt.Sprintf("%t", n3)
	fmt.Printf("s3对应的类型是: %T, s3 = %q \n", s3, s3)

	var s4 string = fmt.Sprintf("%C", n4)
	fmt.Printf("s4对应的类型是: %T, s4 = %q \n", s4, s4)

	// 第二个案例
	var num01 int = 18
	var s01 string = strconv.FormatInt(int64(num01), 10)
	fmt.Printf("s01对应的类型是: %T, s01 = %q \n", s01, s01)
	fmt.Println(num01)
	fmt.Printf("num01的默认类型是: %T", strconv.FormatInt(int64(num01), 10))

	fmt.Println()
	var num02 float64 = 4.23
	var s02 string = strconv.FormatFloat(num02, 'f', 9, 64)
	fmt.Printf("s02对应的类型是: %T, s01 = %q \n", s02, s02)

}
